{
"Document": {
"CstmrCdtTrfInitn": {
"GrpHdr": {
"MsgId": "MsgID283615525808001",
"CreDtTm": "2020-08-25T18:12:05",
"NbOfTxs": "1",
"CtrlSum": "19.0",
"InitgPty": {
"Nm": "CBX TEST FF",
"Id": {
"OrgId": {
"Othr": {
"Id": "99000003"
}
}
}
}
},
"PmtInf": [
{
"PmtInfId": "CBXBATCH001",
"PmtMtd": "TRF",
"NbOfTxs": "1",
"CtrlSum": "19.0",
"PmtTpInf": {
"SvcLvl": {
"Prtry": "FULFILL_REQUEST_FOR_PAYMENT"
},
"LclInstrm": {
"Prtry": "BTR"
}
},
"ReqdExctnDt": "2020-08-25",
"Dbtr": {
"Nm": "FF ACC 2",
"PstlAdr": {
"Ctry": "CA"
},
"Id": {
"OrgId": {
"Othr": {
"Id": "99000003"
}
}
}
},
"DbtrAcct": {
"Ccy": "CAD",
"Nm": "FF ACC 2",
"Id": {
"Othr": {
"Id": "9000024"
}
}
},
"DbtrAgt": {
"FinInstnId": {
"Othr": {
"Id": "0010"
}
},
"BrnchId": {
"Id": "6001"
}
},
"CdtTrfTxInf": [
{
"PmtId": {
"EndToEndId": "E2E200204131489498361556",
"InstrId": "INT2930482030184839298361556"
},
"Amt": {
"InstdAmt": {
"Ccy": "CAD",
"Amt": "19.0"
}
},
"Cdtr": {
"Nm": "Aka_lostGirl",
"PstlAdr": {
"Ctry": "IN"
}
},
"SplmtryData": {
"Envlp": {
"Cnts": {
"StlmMth": "A",
"ChnlPymtId": "88941700619-2308202001",
"DbAmt": "19.0",
"Ccy": "CAD",
"ProductCd": "DOMESTIC",
"DbtrPuId": "99000003",
"PymtExpDt": "2020-06-07T06:12:05.210Z",
"AcctCretDt": "2020-07-06"
}
}
}
}
]
}
],
"SplmtryData": {
"Envlp": {
"Cnts": {
"PANID": "9000000000000005",
"ChnlIndctr": "ONLINE",
"CustIpAddr": "000.000.000.000",
"CustAuthMtd": "PASSWORD",
"AuthTp": "NOT_REQUIRED"
}
}
}
}
}
}