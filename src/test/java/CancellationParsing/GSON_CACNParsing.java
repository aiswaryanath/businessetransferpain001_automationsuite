package CancellationParsing;

import java.io.FileWriter;
import java.io.IOException;

import org.json.JSONException;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Connection;
import com.codoid.products.fillo.Fillo;
import com.codoid.products.fillo.Recordset;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;

import Api_Operaitons.POST_requestApi;
import Assgnmt.Agt;
import Assgnmt.Assgne;
import Assgnmt.Assgnmt;
import Assgnmt.Assgnr;
import Assgnmt.CtrlData;
import Assgnmt.FinInstnId;
import Assgnmt.Pty;
import Busness_Validation.TxnProcOpnValidation;
import InitgPty.Id;
import InitgPty.OrgId;
import InitgPty.Othr;
import JSONStructure_PAIN001.GrpHdr;
import ParseRecordExcel.Update_ExcelResult;
import PaymentCACN.CancelAPI;
import PaymentCACN.CstmrPmtCxlReq;
import PaymentCACN.DocumentCACN;
import PaymentCACN.RootCACN;
import ReadDBConfiguration.ReadDB;
import SplmtryData.Cnts;
import SplmtryData.Envlp;
import SplmtryData.SplmtryData;
import Undrlyg.Case;
import Undrlyg.Cretr;
import Undrlyg.CxlRsnInf;
import Undrlyg.OrgnlInstdAmt;
import Undrlyg.OrgnlPmtInfAndCxl;
import Undrlyg.OrgnlReqdExctnDt;
import Undrlyg.TxInf;
import Undrlyg.Undrlyg;
import Utility.SaltString;

public class GSON_CACNParsing {
	static String _API_Link = ReadDB.getAPI_URL();
	static String channelId = ReadDB.getchannelId();
	static String service_type = ReadDB.getservice_type();
	static String flow_id = ReadDB.getflow_id();
	static String CACN_API_URL = ReadDB.getCACN_id();
	
	public static void Cancellation(Recordset rs,String TESTCASEID) throws IOException, JSONException, FilloException {
		DocumentCACN Document = new DocumentCACN();  
		CstmrPmtCxlReq CstmrPmtCxlReq=new CstmrPmtCxlReq();

//Assgnmt
		Assgnmt Assgnmt=new Assgnmt();
		Pty _Pty=new Pty();
		Id Id=new Id();
		
		OrgId OrgId=new OrgId();
		Othr Othr=new Othr();
		if((!rs.getField("OrgId_value").isEmpty()))
		{		
		Othr.setId(rs.getField("OrgId_value"));
		}
		if((!rs.getField("Nm").isEmpty()))
		{		
		_Pty.setNm(rs.getField("Nm"));
		}
		OrgId.setOthr(Othr);
		Id.setOrgId(OrgId);
		
		_Pty.setId(Id);
		Assgnr Assgnr=new Assgnr();
		Assgnr.setPty(_Pty);
		
		Assgnmt.setAssgnr(Assgnr);
		
		Assgnmt.setId("CACN"+SaltString.getSaltString(5));
		
		Assgne asgne=new Assgne();
		Agt agt=new Agt();
		FinInstnId fin=new FinInstnId();
		if((!rs.getField("BICFI").isEmpty()))
		{		
		fin.setBICFI(rs.getField("BICFI"));
		}
		agt.setFinInstnId(fin);
		asgne.setAgt(agt);
		Assgnmt.setAssgne(asgne);
		if((!rs.getField("CreDtTm").isEmpty()))
		{		
			Assgnmt.setCreDtTm(rs.getField("CreDtTm"));
		}
		
		CstmrPmtCxlReq.setAssgnmt(Assgnmt);
		
		CtrlData CtrlData=new CtrlData();
		if((!rs.getField("CtrlSum").isEmpty()))
		{		
			CtrlData.setCtrlSum(rs.getField("CtrlSum"));
		}
		if((!rs.getField("NbOfTxs").isEmpty()))
		{		
			CtrlData.setNbOfTxs(rs.getField("NbOfTxs"));
		}
		
		CstmrPmtCxlReq.setCtrlData(CtrlData);
		
		
		
		Undrlyg Undrlyg=new Undrlyg();
		OrgnlPmtInfAndCxl OrgnlPmtInfAndCxl=new OrgnlPmtInfAndCxl();
		if((!rs.getField("OrgnlPmtInfId").isEmpty()))
		{	
		OrgnlPmtInfAndCxl.setOrgnlPmtInfId(rs.getField("OrgnlPmtInfId"));
		}
		if((!rs.getField("OrgnlMsgId").isEmpty()))
		{	
		OrgnlPmtInfAndCxl.setOrgnlMsgId(rs.getField("OrgnlMsgId"));
		}
		if((!rs.getField("OrgnlMsgNmId").isEmpty()))
		{	
		OrgnlPmtInfAndCxl.setOrgnlMsgNmId(rs.getField("OrgnlMsgNmId"));
		}
		if((!rs.getField("GrpCxl").isEmpty()))
		{	
		OrgnlPmtInfAndCxl.setGrpCxl(rs.getField("GrpCxl"));
		}
		if((!rs.getField("PmtInfCxl").isEmpty()))
		{	
		OrgnlPmtInfAndCxl.setPmtInfCxl(rs.getField("PmtInfCxl"));
		}
		
		TxInf TxInf=new TxInf();
		if((!rs.getField("CxlId").isEmpty()))
		{	
			TxInf.setCxlId(rs.getField("CxlId"));
		}
	
		//System.out.println("CANCELAPI::"+CancelAPI.GetEndToEndID(TESTCASEID).get(0));
		Case case1=new Case();
		if((!rs.getField("CaseId").isEmpty()))
		{	
			case1.setId(rs.getField("CaseId"));
		}
		
		if((!rs.getField("OrgnlEndToEndId").isEmpty()))
		{	
			TxInf.setOrgnlEndToEndId(rs.getField("OrgnlEndToEndId"));
		}
		
	
		
		Pty Pty=new Pty(); 
		Cretr Cretr=new Cretr();
		
	
		Id Id1=new Id();
		OrgId OrgId1=new OrgId();
		Othr Othr1=new Othr();
		if((!rs.getField("OthrId").isEmpty()))
		{	
			Othr1.setId(rs.getField("OthrId"));
		}
	
		OrgId1.setOthr(Othr1);
		Id1.setOrgId(OrgId1);
		
		Pty.setId(Id1);
		if((!rs.getField("PtyNm").isEmpty()))
		{	
			Pty.setNm(rs.getField("PtyNm"));
		}
		OrgId1.setOthr(Othr1);
		Id1.setOrgId(OrgId1);
		
		Cretr.setPty(Pty);
		case1.setCretr(Cretr);
		
		OrgnlInstdAmt OrgnlInstdAmt=new OrgnlInstdAmt();
		if((!rs.getField("Ccy").isEmpty()))
		{	
			OrgnlInstdAmt.setCcy(rs.getField("Ccy"));
		}
		if((!rs.getField("text").isEmpty()))
		{	
			OrgnlInstdAmt.setText(rs.getField("text"));
		}
		
		CxlRsnInf CxlRsnInf=new CxlRsnInf();
		if((!rs.getField("AddtlInf").isEmpty()))
		{	
		CxlRsnInf.setAddtlInf(rs.getField("AddtlInf"));
		}
		TxInf.setCxlRsnInf(CxlRsnInf);
		TxInf.setCase(case1);
		OrgnlReqdExctnDt odt=new OrgnlReqdExctnDt();
		
		if((!rs.getField("OrgnlReqdExctnDt").isEmpty()))
		{	
			odt.setDt(rs.getField("OrgnlReqdExctnDt"));
		}
		TxInf.setOrgnlReqdExctnDt(odt);
		TxInf.setOrgnlInstdAmt(OrgnlInstdAmt);
		if((!rs.getField("OrgnlInstrId").isEmpty()))
		{	
			TxInf.setOrgnlInstrId(rs.getField("OrgnlInstrId"));
		}
		if((!rs.getField("OrgnlEndToEndId").isEmpty()))
		{	
			TxInf.setOrgnlEndToEndId(rs.getField("OrgnlEndToEndId"));
		}
		
		
		//TxInf add here
		OrgnlPmtInfAndCxl.setTxInf(TxInf);	
		
		Undrlyg.setOrgnlPmtInfAndCxl(OrgnlPmtInfAndCxl);
		 SplmtryData sp =new  SplmtryData();
		 Envlp env=new Envlp(); 
		 Cnts cnt=new Cnts();
		 if((!rs.getField("ClrSysRef").isEmpty()))
			{	
				cnt.setClrSysRef(rs.getField("ClrSysRef"));
			}
			env.setCnts(cnt);
			sp.setEnvlp(env);
		 
		CstmrPmtCxlReq.setUndrlyg(Undrlyg);
		CstmrPmtCxlReq.setSp(sp);	
		Document.setCstmrPmtCxlReq(CstmrPmtCxlReq);
		
		RootCACN RootCACN=new RootCACN();
		RootCACN.setDocumentCACN(Document);
		
		Gson Gson =new GsonBuilder().setPrettyPrinting().create();
		JsonElement str=Gson.toJsonTree(RootCACN);
		String prettyJsonString = Gson.toJson(str);
		System.out.println("req payload:"+prettyJsonString);

	     FileWriter fw=new FileWriter("D:\\BusinessETransfer_Automation\\_Output\\"+TESTCASEID+"_CACN_Pain001_REQ.json");    
           fw.write(prettyJsonString);    
         fw.close();
	}
	public static void main(String[] args) throws Exception{
		//Cancellation("Cancellation_BET");
		
		Fillo fill1=new Fillo();
		Connection conn=fill1.getConnection("D:/BusinessETransfer_Automation/Test_Data/AutomationTest_dataPain001 - CANCELLATION.xlsx");
		Recordset rs=conn.executeQuery("Select * from PAIN001 where RUN_CASE = 'Y'");
			 while (rs.next()) {
			
				String TestCaseID = rs.getField("Test_ID");
				System.out.println(TestCaseID);
					  Cancellation(rs,TestCaseID);
					POST_requestApi.ExecuteCancellationAPI(TestCaseID,CACN_API_URL,channelId,service_type,flow_id);
					Thread.sleep(5000);
					Update_ExcelResult.Update_DataReportAckCanc(TestCaseID, TxnProcOpnValidation.getStatusForCACN(rs.getField("Id")), "CANCEL_STATUS");
//				}
			}
		}

	}
	

