package Undrlyg;

public class OrgnlPmtInfAndCxl {
	private String OrgnlPmtInfId;
	private String OrgnlMsgId;
	private String OrgnlMsgNmId;
	private String GrpCxl;
	private String PmtInfCxl;
	
	private TxInf TxInf;
	

	public TxInf getTxInf() {
		return TxInf;
	}

	public void setTxInf(TxInf txInf) {
		TxInf = txInf;
	}

	public String getOrgnlPmtInfId() {
		return OrgnlPmtInfId;
	}

	public void setOrgnlPmtInfId(String orgnlPmtInfId) {
		OrgnlPmtInfId = orgnlPmtInfId;
	}
	public String getOrgnlMsgId() {
		return OrgnlMsgId;
	}

	public void setOrgnlMsgId(String orgnlMsgId) {
		OrgnlMsgId = orgnlMsgId;
	}

	public String getOrgnlMsgNmId() {
		return OrgnlMsgNmId;
	}

	public void setOrgnlMsgNmId(String orgnlMsgNmId) {
		OrgnlMsgNmId = orgnlMsgNmId;
	}

	public String getGrpCxl() {
		return GrpCxl;
	}

	public void setGrpCxl(String grpCxl) {
		GrpCxl = grpCxl;
	}

	public String getPmtInfCxl() {
		return PmtInfCxl;
	}

	public void setPmtInfCxl(String pmtInfCxl) {
		PmtInfCxl = pmtInfCxl;
	}

	
}
