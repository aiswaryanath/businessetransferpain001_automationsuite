package Undrlyg;

public class TxInf {
	private String CxlId;
	private Case Case;
	private String OrgnlInstrId;
	private String OrgnlEndToEndId;
	private OrgnlInstdAmt OrgnlInstdAmt;
	private OrgnlReqdExctnDt OrgnlReqdExctnDt;
	private CxlRsnInf CxlRsnInf;
	
	public OrgnlInstdAmt getOrgnlInstdAmt() {
		return OrgnlInstdAmt;
	}
	public void setOrgnlInstdAmt(OrgnlInstdAmt orgnlInstdAmt) {
		OrgnlInstdAmt = orgnlInstdAmt;
	}
	public OrgnlReqdExctnDt getOrgnlReqdExctnDt() {
		return OrgnlReqdExctnDt;
	}
	public void setOrgnlReqdExctnDt(OrgnlReqdExctnDt orgnlReqdExctnDt) {
		OrgnlReqdExctnDt = orgnlReqdExctnDt;
	}
	public CxlRsnInf getCxlRsnInf() {
		return CxlRsnInf;
	}
	public void setCxlRsnInf(CxlRsnInf cxlRsnInf) {
		CxlRsnInf = cxlRsnInf;
	}
	public String getCxlId() {
		return CxlId;
	}
	public void setCxlId(String cxlId) {
		CxlId = cxlId;
	}
	
	public Case getCase() {
		return Case;
	}
	public void setCase(Case case1) {
		Case = case1;
	}
	public String getOrgnlInstrId() {
		return OrgnlInstrId;
	}
	public void setOrgnlInstrId(String orgnlInstrId) {
		OrgnlInstrId = orgnlInstrId;
	}
	public String getOrgnlEndToEndId() {
		return OrgnlEndToEndId;
	}
	public void setOrgnlEndToEndId(String orgnlEndToEndId) {
		OrgnlEndToEndId = orgnlEndToEndId;
	}
	
}
