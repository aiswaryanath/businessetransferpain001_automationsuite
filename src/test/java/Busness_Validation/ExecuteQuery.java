package Busness_Validation;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ExecuteQuery {
	
	public static ResultSet getResultSet(final Connection connection,final String query) {
	    System.out.println(""+query);
	    return executeQuery(connection, query);
	}

	private static final ResultSet executeQuery(final Connection connection, final String query) {
	    try {
	        return connection.createStatement().executeQuery(query);
	    } catch (SQLException e) {
	        throw new RuntimeException(e);
	    }
	}
}
