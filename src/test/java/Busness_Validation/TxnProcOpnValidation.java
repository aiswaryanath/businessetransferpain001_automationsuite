package Busness_Validation;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import Api_Operaitons.POST_requestApi;
import ParseRecordExcel.Update_ExcelResult;
import PaymentCACN.GSON_CACNDataMainipulation;
import ReadDBConfiguration.ReadDB;

public class TxnProcOpnValidation {

	public static void getdata_TxnProcOpn(String TestCaseID,String WorkItemId,String Status) throws Exception{
		try{
				Class.forName(ReadDB.getENV1_OracleDriver());
				Connection connection=DriverManager.getConnection(ReadDB.getENV1_ConnName(),ReadDB.getENV1_Uname(),ReadDB.getENV1_Pass());
				ResultSet LHS_ExceptionResult=ExecuteQuery.getResultSet(connection, "Select WORKITEMID,FILE_WORKITEM_ID,cust_id,txns_oti09,TXN_STATUS,WIRE_EXT_REF_NUM,SRC_REF_NUM,TRN_STATUS_CODE FROM TXN_PROC_OPN WHERE FILE_WORKITEM_ID IN ("+WorkItemId+")");
		   try {
		    	  while(LHS_ExceptionResult.next()) {
	    			String TXN_Status = LHS_ExceptionResult.getString("TXN_STATUS");
	    			String WORKITEMID = LHS_ExceptionResult.getString("WORKITEMID");
	       			System.out.println("WORKITEMID:"+WORKITEMID+"\t"+"TXN_Status:"+TXN_Status);
    				//Async Resp
    				psh_response_messagesValidation.Getpsh_response_messages(TestCaseID, WORKITEMID,"TRXN");
    				Update_ExcelResult.Update_DataReportAck(TestCaseID, TXN_Status, "TXN_StatusActual");
    				
//    				TXN_StatusActual
		    			Update_ExcelResult.Update_DataReportAck(TestCaseID, WORKITEMID, "PaymentWorkItem_ID");
		    			
		    			if (TXN_Status.equalsIgnoreCase("Payment Rejected")) {
		    				GtbExcepGridDetailvalidation.GtbExcepGridDetail(TestCaseID, WORKITEMID);
						}
		    			if (TXN_Status.equalsIgnoreCase("Payment Suspended")) {
		    				GtbExcepGridDetailvalidation.GtbExcepGridDetail(TestCaseID, WORKITEMID);
						}
				  }
				}catch (Exception e) {
						e.printStackTrace();
				}finally {
					connection.close();
					LHS_ExceptionResult.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
	}

public static String getStatusForCACN(String Message_ID) throws Exception{
	String TXN_STATUS = null;
	try{
				Class.forName(ReadDB.getENV1_OracleDriver());
				Connection connection=DriverManager.getConnection(ReadDB.getENV1_ConnName(),ReadDB.getENV1_Uname(),ReadDB.getENV1_Pass());
				ResultSet LHS_ExceptionResult=ExecuteQuery.getResultSet(connection, "Select WORKITEMID,FILE_WORKITEM_ID,cust_id,txns_oti09,TXN_STATUS,WIRE_EXT_REF_NUM,SRC_REF_NUM,TRN_STATUS_CODE FROM TXN_PROC_OPN WHERE FILE_WORKITEM_ID IN (select workitemid from file_proc_opn where message_id = '"+Message_ID+"')");
		   try {
		    	  while(LHS_ExceptionResult.next()) {
		    		  TXN_STATUS = LHS_ExceptionResult.getString("TXN_STATUS");
	    			System.out.println(TXN_STATUS);
				  }
				}catch (Exception e) {
						e.printStackTrace();
				}finally {
					connection.close();
					LHS_ExceptionResult.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
		}
	return TXN_STATUS;
}

public static boolean RecusriveStatusCheck(String Message_ID,String EXPECTED_STATUS){
	boolean flag=false;
	try{
	int count =0;
	 while(count<10){
		 if (getStatusForCACN(Message_ID).equals(EXPECTED_STATUS)) {
			 System.out.println("Element found!!");
			 flag=true;
			 break;
		}else{
			System.out.println(count);
			count ++;
		}
	 }
}catch (Exception e) {
	System.out.println("Exception checking again!!"+e.toString());
}
	return flag;
}
	public static void main(String[] args) throws Exception{
/*		if(RecusriveStatusCheck("BET8NVQZSLVBE", "Payment Recalledaa")==true){
			System.out.println("TRUE");
		}
*/
//		Update_ExcelResult.Update_DataReportAck("BTR_US902_TC248", TxnProcOpnValidation.getStatusForCACN("BETXE45QD7N2E"), "CANCEL_STATUS");
		
/*		if (TxnProcOpnValidation.RecusriveStatusCheck("BETV1VUZ61EY6", "Settlement In Progress")==true) {
			GSON_CACNDataMainipulation.Cancellation("BTR_US902_TC248");
			POST_requestApi.ExecuteCancellationAPI("BTR_US902_TC248","http://10.198.12.11:20004/APIHandler/api/cancelPayment","CBX","","");
			Thread.sleep(60000);
			Update_ExcelResult.Update_DataReportAck("BTR_US902_TC248", TxnProcOpnValidation.getStatusForCACN("BETXE45QD7N2E"), "CANCEL_STATUS");
		}
*/
		
	}
}