package Cdtr;

import java.util.ArrayList;

public class CdtrPstlAdr {
	private String StrtNm;
	private String StrtNmX;
	
	public String getStrtNmX() {
		return StrtNmX;
	}

	public void setStrtNmX(String strtNmX) {
		StrtNmX = strtNmX;
	}

	private String Ctry;
	private String CtryX;
	
	public String getCtryX() {
		return CtryX;
	}

	public void setCtryX(String ctryX) {
		CtryX = ctryX;
	}

	private ArrayList AdrLine;
	
	public ArrayList getAdrLine() {
		return AdrLine;
	}

	public void setAdrLine(ArrayList adrLine) {
		AdrLine = adrLine;
	}

	public String getStrtNm() {
		return StrtNm;
	}

	public void setStrtNm(String strtNm) {
		StrtNm = strtNm;
	}

	public String getCtry() {
		return Ctry;
	}

	public void setCtry(String ctry) {
		Ctry = ctry;
	}
	
}
