package Cdtr;
public class Cdtr {
	
	private String Nm;
	private String NmX;
	private CdtrPstlAdr PstlAdr;
	private CtctDtls CtctDtls;
	
	public CtctDtls getCtctDtls() {
		return CtctDtls;
	}
	public void setCtctDtls(CtctDtls ctctDtls) {
		CtctDtls = ctctDtls;
	}
	public String getNmX() {
		return NmX;
	}
	public void setNmX(String nmX) {
		NmX = nmX;
	}
	public String getNm() {
		return Nm;
	}
	public void setNm(String nm) {
		Nm = nm;
	}
	public CdtrPstlAdr getPstlAdr() {
		return PstlAdr;
	}
	public void setPstlAdr(CdtrPstlAdr pstlAdr) {
		PstlAdr = pstlAdr;
	}
	
}
