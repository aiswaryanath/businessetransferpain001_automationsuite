package PARSE_CACN;

import java.io.BufferedReader;
import java.io.FileReader;

import org.json.JSONObject;

import Busness_Validation.TxnProcOpnValidation;
import ParseJSON.StructureValidations;
import ParseRecordExcel.Update_ExcelResult;

public class CancelStructureValidations {
	
	
	public static String readFile(String filename) {
	    String result = "";
	    try {
	        BufferedReader br = new BufferedReader(new FileReader(filename));
	        StringBuilder sb = new StringBuilder();
	        String line = br.readLine();
	        while (line != null) {
	            sb.append(line);
	            line = br.readLine();
	        }
	        result = sb.toString();
	    } catch(Exception e) {
	        e.printStackTrace();
	    }
	    return result;
	}
	
	public static void CancelJSON_StructureValidations(String TestCaseID) {
		try{
		String toString_RespJSON_Structureval = readFile("D:\\BusinessETransfer_Automation\\_Output\\"+TestCaseID+"CACN_pain001_RESP.json");
		JSONObject json_object=new JSONObject(toString_RespJSON_Structureval);
		
		String Document = json_object.getJSONObject("Document").toString(); 
		JSONObject Document_ob=new JSONObject(Document);
		
		String RsltnOfInvstgtn_ob_str = Document_ob.getString("RsltnOfInvstgtn");
		
		System.out.println(RsltnOfInvstgtn_ob_str);
		
//		Assgnmt
		JSONObject Assgnmt_obj = new JSONObject(RsltnOfInvstgtn_ob_str);
		String Assgnmt_Str = Assgnmt_obj.get("Assgnmt").toString();
		System.out.println(Assgnmt_Str);
		
		//Sts
		JSONObject Sts_CANCEL_obj = new JSONObject(RsltnOfInvstgtn_ob_str);
		String Sts_CANCEL_str = Sts_CANCEL_obj.get("Sts").toString();
		System.out.println(Sts_CANCEL_str);
		
		JSONObject _CONF_obj = new JSONObject(Sts_CANCEL_str);
		String _CONF_str = _CONF_obj.get("Conf").toString();
		System.out.println(_CONF_str);
		
//		ASYNC_RESPCANCEL_Actual
		Update_ExcelResult.Update_DataReportAckCanc(TestCaseID, _CONF_str, "ASYNC_RESPCANCEL_Actual");
		
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}

	public static void main(String[] args) {
		CancelJSON_StructureValidations("CANCELLATION_POSITIVE");
	}
	
}
