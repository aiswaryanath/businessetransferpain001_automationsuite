package Amt;

public class Amt {
	private InstdAmt InstdAmt;
	private EqvtAmt EqvtAmt;
	
	public EqvtAmt getEqvtAmt() {
		return EqvtAmt;
	}
	public void setEqvtAmt(EqvtAmt eqvtAmt) {
		EqvtAmt = eqvtAmt;
	}
	public InstdAmt getInstdAmt() {
		return InstdAmt;
	}
	public void setInstdAmt(InstdAmt instdAmt) {
		InstdAmt = instdAmt;
	}
	
}
