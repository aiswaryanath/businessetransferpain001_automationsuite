package Amt;

public class InstdAmt {
	private String Ccy;
	private String CcyX;
	
	private String Amt;
	private String AmtX;
	
	public String getCcyX() {
		return CcyX;
	}
	public void setCcyX(String ccyX) {
		CcyX = ccyX;
	}
	public String getAmtX() {
		return AmtX;
	}
	public void setAmtX(String amtX) {
		AmtX = amtX;
	}
	public String getCcy() {
		return Ccy;
	}
	public void setCcy(String ccy) {
		Ccy = ccy;
	}
	public String getAmt() {
		return Amt;
	}
	public void setAmt(String amt) {
		Amt = amt;
	}
}
