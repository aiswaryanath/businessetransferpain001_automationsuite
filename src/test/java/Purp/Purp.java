package Purp;

import org.codehaus.jackson.annotate.JsonPropertyOrder;

@JsonPropertyOrder({"Prtry","PrtryX"})
public class Purp {
	private String Prtry;
	private String PrtryX;

	public String getPrtryX() {
		return PrtryX;
	}

	public void setPrtryX(String prtryX) {
		PrtryX = prtryX;
	}

	public String getPrtry() {
		return Prtry;
	}

	public void setPrtry(String prtry) {
		Prtry = prtry;
	}
}
