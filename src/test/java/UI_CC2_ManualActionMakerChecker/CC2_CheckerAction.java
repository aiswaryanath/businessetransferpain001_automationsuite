package UI_CC2_ManualActionMakerChecker;

import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CC2_CheckerAction {

	public static void CheckerExecute(WebDriver driver ,String Workitemid) throws Exception {

		Thread.sleep(10000);
		driver.switchTo().defaultContent();
		WebElement eleFrame=driver.findElement(By.xpath("//frame[@name='TabFrame']"));
		driver.switchTo().frame(eleFrame);
		
		System.out.println((String) ((JavascriptExecutor) driver).executeScript("return window.frameElement.name"));
		Thread.sleep(2000);
//		driver.findElement(By.xpath("(//*[text()='Authorization'])[last()-1]")).click();
		
		WebElement ele_Auth = driver.findElement(By.xpath("(//span/span[text()='Authorization'])"));
		Actions ref_Auth = new Actions(driver);
		ref_Auth.moveToElement(ele_Auth);
		ref_Auth.click().build().perform();
		
		driver.switchTo().defaultContent();

		driver.switchTo().frame("MenuFrame");
		driver.findElement(By.xpath("//div[text()='Wires']")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[text()='Suspense']")).click();
		Thread.sleep(2000);
//		 driver.findElement(By.xpath("//*[text()='UMM - Response Error Codes']")).click();
		 driver.findElement(By.xpath("//*[text()='IWS Timeout']")).click();
		 driver.switchTo().defaultContent();

		// Click On Search WorkitemID
		Thread.sleep(5000);

		driver.switchTo().frame("SearchFrame");
		String currentFrameName2 = (String) ((JavascriptExecutor) driver).executeScript("return window.frameElement.name");
		System.out.println("currentFrameName2" + currentFrameName2);

		WebElement Search_WitemId = driver.findElement(By.xpath("(//*[@class=' x-form-text x-form-field x-box-item'])[last()-24]"));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].value='"+Workitemid+"';", Search_WitemId);

		((JavascriptExecutor) driver).executeScript("arguments[0].click();",
				driver.findElement(By.xpath("(//*[@class='x-grid3-hd-btn'])[last()-25]")));
		Thread.sleep(1000);

		WebElement ele = driver.findElement(By.xpath("(//*[@class='x-menu-list-item'])[last()-0]"));
		Actions ref = new Actions(driver);
		ref.moveToElement(ele);
		ref.click().build().perform();

		WebElement ele1 = driver.findElement(By.xpath("(//*[@class='x-menu-item-icon '])[last()-7]"));
		Actions ref1 = new Actions(driver);
		ref1.moveToElement(ele1);
		ref1.click().build().perform();

		Thread.sleep(3000);

		driver.findElement(By.xpath("(//*[contains(text(),'"+Workitemid+"')])[last()-1]")).click();

		driver.switchTo().defaultContent();

		Thread.sleep(10000);
		System.out.println("After Click on Work Item ID:" + driver.getTitle());

		Set<String> winHandles = driver.getWindowHandles();
		// Loop through all handles
		for (String handle : winHandles) {
			driver.switchTo().window(handle);
			Thread.sleep(5000);
			System.out.println("Title of the new window: " + driver.getTitle());
			if (driver.getTitle().equalsIgnoreCase("Label Entry Window")) {
				driver.switchTo().window(handle);
				System.out.println("Switch Successfull!!: " + driver.getTitle());

				System.out.println("Check Switch is Successfull:" + driver.getTitle());

				// IFrame_WK_TAB_270308
				Thread.sleep(2000);

				WebElement frame1 = driver.findElement(By.xpath("//div[@id='TAB_0-body']//iframe"));
				driver.switchTo().frame(frame1);
				System.out.println("Parent Frame:"
						+ (String) ((JavascriptExecutor) driver).executeScript("return window.frameElement.name"));

				driver.switchTo().frame("IwMiddleFrame");
				System.out.println("IwMiddleFrame:"
						+ (String) ((JavascriptExecutor) driver).executeScript("return window.frameElement.name"));

				((JavascriptExecutor) driver).executeScript("arguments[0].click();",
						driver.findElement(By.xpath("(//*[@id='PEAL'])[last()-4]")));

				((JavascriptExecutor) driver).executeScript("arguments[0].click();",
						driver.findElement(By.xpath("(//*[@id='P162943174-TD336'])")));

				((JavascriptExecutor) driver).executeScript("arguments[0].click();",
						driver.findElement(By.xpath("(//*[@id='PEAL'])[last()]")));

				driver.findElement(By.id("P162943174-331294617")).sendKeys("Send Maker");

				driver.findElement(By.id("P162943174-565944340")).click();

				driver.switchTo().defaultContent();

				WebElement frame2 = driver.findElement(By.xpath("//div[@id='TAB_0-body']//iframe"));
				driver.switchTo().frame(frame2);
				System.out.println("Parent Frame:"
						+ (String) ((JavascriptExecutor) driver).executeScript("return window.frameElement.name"));

				driver.switchTo().frame("IwBottomFrame");
				System.out.println("IwBottomFrame:"
						+ (String) ((JavascriptExecutor) driver).executeScript("return window.frameElement.name"));

				((JavascriptExecutor) driver).executeScript("arguments[0].click();",
						driver.findElement(By.id("P162943174-524972874")));
				driver.quit();
			}
		}
	}
}
