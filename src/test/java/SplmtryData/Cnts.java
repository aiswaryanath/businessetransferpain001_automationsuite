//aiswarya added new fields as part of
package SplmtryData;

public class Cnts {
	private String StlmMth;
	private String PymtAmt;
	private String AutoDep;
	private String RTPFlflmntInd;
	private String securityQuestion;
	private String securityAnswer;
	private String hashType;
	private String hashSalt;
	private String ChnlIndctr;
	private String CustIpAddr;
	private String CustAuthMtd;
	private String AuthTp;
	private String PANID;
	private String FXId;
	private String channelSeqId;
	private String ProductCd;
	private String DbtrPuId;
	private String PymtExpDt;
	private String ChnlPymtId;
	private String Ccy;
	private String DbAmt;
	private String originatorFIRTPRefNum;
	private String interacRTPRefNum;
	private String PmtRlsDt;
	private String AcctCretDt;
	private String chargesAmount;
	private String ClrSysRef;
	
	public String getPmtRlsDt() {
		return PmtRlsDt;
	}

	public void setPmtRlsDt(String pmtRlsDt) {
		PmtRlsDt = pmtRlsDt;
	}

	public String getClrSysRef() {
		return ClrSysRef;
	}

	public void setClrSysRef(String clrSysRef) {
		ClrSysRef = clrSysRef;
	}

	public String getChargesAmount() {
		return chargesAmount;
	}

	public void setChargesAmount(String chargesAmount) {
		this.chargesAmount = chargesAmount;
	}

	public String getAcctCretDt() {
		return AcctCretDt;
	}

	public String getPymtRelDt() {
		return PmtRlsDt;
	}
	public void setPymtRelDt(String pymtRelDt) {
		PmtRlsDt = pymtRelDt;
	}
	public String getOriginatorFIRTPRefNum() {
		return originatorFIRTPRefNum;
	}
	public void setOriginatorFIRTPRefNum(String originatorFIRTPRefNum) {
		this.originatorFIRTPRefNum = originatorFIRTPRefNum;
	}
	public String getInteracRTPRefNum() {
		return interacRTPRefNum;
	}
	public void setInteracRTPRefNum(String interacRTPRefNum) {
		this.interacRTPRefNum = interacRTPRefNum;
	}
	public String getPANID() {
		return PANID;
	}
	public void setPANID(String pANID) {
		PANID = pANID;
	}
	public String getFXId() {
		return FXId;
	}
	public void setFXId(String fXId) {
		FXId = fXId;
	}
	public String getChannelSeqId() {
		return channelSeqId;
	}
	public void setChannelSeqId(String channelSeqId) {
		this.channelSeqId = channelSeqId;
	}
	public String getStlmMth() {
		return StlmMth;
	}
	public void setStlmMth(String stlmMth) {
		StlmMth = stlmMth;
	}
	public String getPymtAmt() {
		return PymtAmt;
	}
	public void setPymtAmt(String pymtAmt) {
		PymtAmt = pymtAmt;
	}
	public String getAutoDep() {
		return AutoDep;
	}
	public void setAutoDep(String autoDep) {
		AutoDep = autoDep;
	}
	public String getRTPFlflmntInd() {
		return RTPFlflmntInd;
	}
	public void setRTPFlflmntInd(String rTPFlflmntInd) {
		RTPFlflmntInd = rTPFlflmntInd;
	}
	public String getSecurityQuestion() {
		return securityQuestion;
	}
	public void setSecurityQuestion(String securityQuestion) {
		this.securityQuestion = securityQuestion;
	}
	public String getSecurityAnswer() {
		return securityAnswer;
	}
	public void setSecurityAnswer(String securityAnswer) {
		this.securityAnswer = securityAnswer;
	}
	public String getHashType() {
		return hashType;
	}
	public void setHashType(String hashType) {
		this.hashType = hashType;
	}
	public String getHashSalt() {
		return hashSalt;
	}
	public void setHashSalt(String hashSalt) {
		this.hashSalt = hashSalt;
	}
	public String getChnlIndctr() {
		return ChnlIndctr;
	}
	public void setChnlIndctr(String chnlIndctr) {
		ChnlIndctr = chnlIndctr;
	}
	public String getCustIpAddr() {
		return CustIpAddr;
	}
	public void setCustIpAddr(String custIpAddr) {
		CustIpAddr = custIpAddr;
	}
	public String getCustAuthMtd() {
		return CustAuthMtd;
	}
	public void setCustAuthMtd(String custAuthMtd) {
		CustAuthMtd = custAuthMtd;
	}
	public String getAuthTp() {
		return AuthTp;
	}
	public void setAuthTp(String authTp) {
		AuthTp = authTp;
	}
	public String getProductCd() {
		return ProductCd;
	}
	public void setProductCd(String productCd) {
		ProductCd = productCd;
	}
	public String getDbtrPuId() {
		return DbtrPuId;
	}
	public void setDbtrPuId(String dbtrPuId) {
		DbtrPuId = dbtrPuId;
	}

	public void setPymtExpDt(String pymtExpDt) {
		PymtExpDt = pymtExpDt;
	}
	public String getChnlPymtId() {
		return ChnlPymtId;
	}
	public void setChnlPymtId(String chnlPymtId) {
		ChnlPymtId = chnlPymtId;
	}
	public String getCcy() {
		return Ccy;
	}
	public void setCcy(String ccy) {
		Ccy = ccy;
	}
	public String getDbAmt() {
		return DbAmt;
	}
	public void setDbAmt(String dbAmt) {
		DbAmt = dbAmt;
	}
	public String getPymtExpDt() {
		return PymtExpDt;
	}
	public void setAcctCretDt(String acctCretDt) {
		AcctCretDt = acctCretDt;
		
	}

	
}
