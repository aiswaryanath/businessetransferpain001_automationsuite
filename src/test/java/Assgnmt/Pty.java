package Assgnmt;

import InitgPty.Id;

public class Pty {
	private String Nm;
	private Id Id;
	
	public Id getId() {
		return Id;
	}

	public void setId(Id id) {
		Id = id;
	}

	public String getNm() {
		return Nm;
	}

	public void setNm(String nm) {
		Nm = nm;
	}
	
}
