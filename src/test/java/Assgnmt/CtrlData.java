package Assgnmt;

public class CtrlData {
	private String NbOfTxs;
	private String CtrlSum;
	
	public String getNbOfTxs() {
		return NbOfTxs;
	}
	public void setNbOfTxs(String nbOfTxs) {
		NbOfTxs = nbOfTxs;
	}
	public String getCtrlSum() {
		return CtrlSum;
	}
	public void setCtrlSum(String ctrlSum) {
		CtrlSum = ctrlSum;
	}
	
}
