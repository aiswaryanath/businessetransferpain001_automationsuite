package Assgnmt;

public class Assgnmt {
	
	private String Id;
	private Assgnr Assgnr;
	private Assgne Assgne;
	private String CreDtTm;
	
	public Assgne getAssgne() {
		return Assgne;
	}
	public void setAssgne(Assgne assgne) {
		Assgne = assgne;
	}
	public Assgnr getAssgnr() {
		return Assgnr;
	}
	public void setAssgnr(Assgnr assgnr) {
		Assgnr = assgnr;
	}
	public String getId() {
		return Id;
	}
	public void setId(String id) {
		Id = id;
	}
	public String getCreDtTm() {
		return CreDtTm;
	}
	public void setCreDtTm(String creDtTm) {
		CreDtTm = creDtTm;
	}
	
	
}
