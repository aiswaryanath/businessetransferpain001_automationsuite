package PushMTFiles;

import org.openqa.selenium.WebDriver;
import com.codoid.products.fillo.Connection;
import com.codoid.products.fillo.Fillo;
import com.codoid.products.fillo.Recordset;
import MT103UI.GetValueMT103;
import MT103UI.TestMT103UI;
import MT199.MT199;
import MT199UI.TestMT199UI;
import MT900UI.MT900;
import MT900UI.TestMT900UI;
import UI_CC2_ManualActionMakerChecker.CC2_LinearScript;

public class MTFileUIAction {
	
   public static void PUSHMTFiles() throws Exception{
	   try{
		System.setProperty("ROW", "2");
		System.out.println("UI Action Automation in progress..");
//		System.out.println("Select * from PAIN001 where File_WorkitemID = '"+File_WorkitemID+"'");
		Fillo fill=new Fillo();
		Connection conn=fill.getConnection(Constants_pack.Constants._ExcelPath);
//		Recordset rs=conn.executeQuery("Select * from PAIN001 where File_WorkitemID = '"+File_WorkitemID+"'");
		Recordset rs=conn.executeQuery("Select * from PAIN001");
		 while (rs.next()) {
		     if (rs.getField("Selenium_UIActions").equals("Y") && rs.getField("SCENARIO").equalsIgnoreCase("PUSH_IWSMT199")) {
		    	 System.out.println("MT File Execution in Progress..");
		    	 Thread.sleep(50000);
		    	 if (GetValueMT103.getdata_TXN_STATUS(rs.getField("File_WorkitemID")).equalsIgnoreCase("Awaiting IWS Acknowledgement")) {
		    		 System.out.println(rs.getField("File_WorkitemID"));
			    	 TestMT103UI.PUSHMT103(rs.getField("File_WorkitemID"));
			    	 Thread.sleep(10000);
		    	 }

				 if (GetValueMT103.getdata_TXN_STATUS(rs.getField("File_WorkitemID")).equalsIgnoreCase("Awaiting IWS Status")) {
					 MT199.CreateFile_MT199(rs.getField("File_WorkitemID"));	
					 TestMT199UI.PUSHMT199();
				}
		     }
		     
		     if (rs.getField("Selenium_UIActions").equals("Y") && rs.getField("SCENARIO").equalsIgnoreCase("PUSH_IWSMT103_ACK")) {
		    	 System.out.println("MT File Execution in Progress..");
		    	 Thread.sleep(50000);
		    	 if (GetValueMT103.getdata_TXN_STATUS(rs.getField("File_WorkitemID")).equalsIgnoreCase("Awaiting IWS Acknowledgement")) {
		    		 System.out.println(rs.getField("File_WorkitemID"));
			    	 TestMT103UI.PUSHMT103(rs.getField("File_WorkitemID"));
			    	 Thread.sleep(10000);
		    	 }
		     }
		     
		     if (rs.getField("Selenium_UIActions").equals("Y") && rs.getField("SCENARIO").equalsIgnoreCase("PUSH_IWSMT900")) {
		    	 System.out.println("MT File Execution in Progress..");
		    	 Thread.sleep(50000);
		    	 if (GetValueMT103.getdata_TXN_STATUS(rs.getField("File_WorkitemID")).equalsIgnoreCase("Awaiting IWS Acknowledgement")) {
		    		 System.out.println(rs.getField("File_WorkitemID"));
			    	 TestMT103UI.PUSHMT103(rs.getField("File_WorkitemID"));
			    	 Thread.sleep(10000);
		    	 }
		    	 
				 if (GetValueMT103.getdata_TXN_STATUS(rs.getField("File_WorkitemID")).equalsIgnoreCase("Awaiting IWS Status")) {
					 MT199.CreateFile_MT199(rs.getField("File_WorkitemID"));	
					 TestMT199UI.PUSHMT199();
					 Thread.sleep(10000);
				}
				 
				 if (GetValueMT103.getdata_TXN_STATUS(rs.getField("File_WorkitemID")).equalsIgnoreCase("Payment Completed")) {
					 MT900.CreateFile_MT900(rs.getField("File_WorkitemID"));	
					 TestMT900UI.PUSHMT900();
					 Thread.sleep(10000);
				}
		     }
		     
		     if (rs.getField("Selenium_UIActions").equals("Y") && rs.getField("SCENARIO").equalsIgnoreCase("PUSH_ACK_MT199")) {
		    	 System.out.println("MT File Execution in Progress..");
		    	 
				 if (GetValueMT103.getdata_TXN_STATUS(rs.getField("File_WorkitemID")).equalsIgnoreCase("Awaiting IWS Acknowledgement")) {
					 MT199.CreateFile_MT199(rs.getField("File_WorkitemID"));	
					 TestMT199UI.PUSHMT199();
					 Thread.sleep(10000);
				 }
				 
		     }

		     
		     if (rs.getField("Selenium_UIActions").equals("Y") && rs.getField("SCENARIO").equalsIgnoreCase("PUSH_ACK_MT199_MT900")) {
		    	 System.out.println("MT File Execution in Progress..");
		    	 
				 if (GetValueMT103.getdata_TXN_STATUS(rs.getField("File_WorkitemID")).equalsIgnoreCase("Awaiting IWS Acknowledgement")) {
					 TestMT103UI.PUSHMT103(rs.getField("File_WorkitemID"));					 
					 Thread.sleep(10000);
				 }
				 
				 if (GetValueMT103.getdata_TXN_STATUS(rs.getField("File_WorkitemID")).equalsIgnoreCase("Awaiting IWS Status")) {
					 MT199.CreateFile_MT199(rs.getField("File_WorkitemID"));	
					 TestMT199UI.PUSHMT199();
					 Thread.sleep(10000);
				}
				 
				 if (GetValueMT103.getdata_TXN_STATUS(rs.getField("File_WorkitemID")).equalsIgnoreCase("Payment Completed")) {
					 MT900.CreateFile_MT900(rs.getField("File_WorkitemID"));	
					 TestMT900UI.PUSHMT900();
					 Thread.sleep(10000);
				}
				 
		     }
		     
		     
		     if (rs.getField("Selenium_UIActions").equals("Y") && rs.getField("SCENARIO").equalsIgnoreCase("PUSH_IWSMT199")) {
		    	 System.out.println("MT File Execution in Progress..");
		    	 Thread.sleep(50000);
		    	 if (GetValueMT103.getdata_TXN_STATUS(rs.getField("File_WorkitemID")).equalsIgnoreCase("Awaiting IWS Acknowledgement")) {
		    		 System.out.println(rs.getField("File_WorkitemID"));
			    	 TestMT103UI.PUSHMT103(rs.getField("File_WorkitemID"));
			    	 Thread.sleep(10000);
		    	 }
		    	 
				 if (GetValueMT103.getdata_TXN_STATUS(rs.getField("File_WorkitemID")).equalsIgnoreCase("Awaiting IWS Status")) {
					 MT199.CreateFile_MT199(rs.getField("File_WorkitemID"));	
					 TestMT199UI.PUSHMT199();
				}
		     }
		     
		     if (rs.getField("Selenium_UIActions").equals("Y") && rs.getField("SCENARIO").equalsIgnoreCase("PerformManual_Actions")) {
		    	 System.out.println("Maker Checker Action Executes :)");
		    	 Thread.sleep(400000);
		    	 WebDriver driver = null;
		    	 if (GetValueMT103.getdata_TXN_STATUS(rs.getField("File_WorkitemID")).equalsIgnoreCase("Payment Suspended")) {
		    		 CC2_LinearScript.Execute_FGT(driver,"BCCMKR1",rs.getField("PaymentWorkItem_ID"));
			    	 Thread.sleep(10000);
		    	 }

				 if (GetValueMT103.getdata_TXN_STATUS(rs.getField("File_WorkitemID")).equalsIgnoreCase("Awaiting Suspense Approval")) {
					 CC2_LinearScript.Execute_FGT(driver,"BCCCKR1",rs.getField("PaymentWorkItem_ID"));;	
					 Thread.sleep(10000);
				}
		     } 
	     }
		}catch (Exception e) {
			System.out.println(""+e.toString());
		}
	}

	/*public static void main(String[] args) throws Exception	{
		MTFileUIAction.PUSHMTFiles();
	}*/
}
