package APIFrameworkGsonController;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.PrintStream;
import java.util.ArrayList;
import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Connection;
import com.codoid.products.fillo.Fillo;
import com.codoid.products.fillo.Recordset;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import APIGsonKeyValueConditions.AuthstnGson;
import APIGsonKeyValueConditions.CdtrAcctGson;
import APIGsonKeyValueConditions.CdtrGson;
import APIGsonKeyValueConditions.ChrgsAcctGson;
import APIGsonKeyValueConditions.ClrSysIdGson;
import APIGsonKeyValueConditions.ClrSysMmbIdGson;
import APIGsonKeyValueConditions.DbtrAccGson;
import APIGsonKeyValueConditions.DbtrAgtBrnchIdGson;
import APIGsonKeyValueConditions.DbtrAgtGson;
import APIGsonKeyValueConditions.DbtrOrgIdGson;
import APIGsonKeyValueConditions.DbtrPstAdlGson;
import APIGsonKeyValueConditions.EqvtAmtGson;
import APIGsonKeyValueConditions.FinInstIdGson;
import APIGsonKeyValueConditions.GetKeyChrBr;
import APIGsonKeyValueConditions.GrpHdrGson;
import APIGsonKeyValueConditions.InitgPtyGson;
import APIGsonKeyValueConditions.InitialPrtyNmGson;
import APIGsonKeyValueConditions.InstdAmtGson;
import APIGsonKeyValueConditions.IntrmyAgtGson;
import APIGsonKeyValueConditions.PmTpInfSvcLvlGson;
import APIGsonKeyValueConditions.PmtIdGson;
import APIGsonKeyValueConditions.PmtInfGsonkeyValue;
import APIGsonKeyValueConditions.RmtInfGson;
import APIGsonKeyValueConditions.XchgRateInfGson;
import APIGsonKeyValueConditions.cdtrAgtpstladdress;
import APIGsonKeyValueConditions.cdtrPstlAddress;
import APIGsonKeyValueConditions.dbtrPstlAddress;
import Amt.Amt;
import Amt.EqvtAmt;
import Amt.InstdAmt;
import Api_Operaitons.POST_requestApi;
import Busness_Validation.TxnProcOpnValidation;
import Cdtr.Cdtr;
import Cdtr.CdtrPstlAdr;
import Cdtr.CtctDtls;
import CdtrAcct.CdtrAcct;
import CdtrAcct.CdtrAcctId;
import CdtrAcct.CdtrAcctOthr;
import CdtrAgt.CdtrAgt;
import CdtrAgt.CdtrAgtPstlAdr;
import CdtrAgt.ClrSysId;
import CdtrAgt.ClrSysMmbId;
import CdtrAgt.FinInstnId;
import ChrgsAcct.ChrgsAcctId;
import ChrgsAcct.ChrgsAcctOthr;
import Constants_pack.Constants;
import DBConnect.DBConnector;
import Dbtr.Dbtr;
import Dbtr.DbtrId;
import Dbtr.DbtrOrgId;
import Dbtr.DbtrOthr;
import Dbtr.PstlAdr;
import DbtrAcct.DbtrAcct;
import DbtrAcct.DbtrAcctId;
import DbtrAcct.DbtrAcctOthr;
import DbtrAgt.DbtrAgt;
import DbtrAgt.DbtrAgtBrnchId;
import DbtrAgt.DbtrAgtFinInstnId;
import DbtrAgt.DbtrAgtOthr;
import DbtrAgt.DbtrAgtPstlAdr;
import InitgPty.Id;
import InitgPty.InitgPty;
import InitgPty.OrgId;
import InitgPty.Othr;
import IntrmyAgt1.IntrmyAgt1;
import IntrmyAgt1.IntrmyAgt1FinInstnId;
import JSONStructure_PAIN001.Authstn;
import JSONStructure_PAIN001.CdtTrfTxInf;
import JSONStructure_PAIN001.CstmrCdtTrfInitn;
import JSONStructure_PAIN001.Document;
import JSONStructure_PAIN001.GrpHdr;
import JSONStructure_PAIN001.PmtInf;
import JSONStructure_PAIN001.RootClass;
import ParseRecordExcel.Update_ExcelResult;
import PaymentCACN.CancelAPI;
import PaymentCACN.GSON_CACNDataMainipulation;
import PmtId.PmtId;
import PmtTpInf.LclInstrm;
import PmtTpInf.PmtTpInf;
import PmtTpInf.SvcLvl;
import Purp.Purp;
import ReadDBConfiguration.ReadDB;
import RmtInf.RmtInf;
import SplmtryData.Cnts;
import SplmtryData.Envlp;
import SplmtryData.SplmtryData;
//import UI_CC2_ManualAction.CC2_LinearScript;
import Utility.ClearCellData;
import Utility.GetSimpleDateFormat;
import Utility.SQL_Runner;
import Utility.SaltString;
import XchgRateInf.XchgRateInf;

public class BETGsonAPI_DataManipulation {
static PrintStream json_console = null;
static String _API_Link = ReadDB.getAPI_URL();
static String channelId = ReadDB.getchannelId();
static String service_type = ReadDB.getservice_type();
static String flow_id = ReadDB.getflow_id();
static String CACN_API_URL = ReadDB.getCACN_id();

/**
 * @param args
 * @throws FileNotFoundException
 * @throws FilloException
 * @throws Exception
 */

public static void main(String[] args) throws FileNotFoundException, FilloException,Exception {
try{
	System.setProperty("ROW", "2");
	System.out.println("CC2 Automation in progress..");
	SQL_Runner.PREExecute_QueriesCC2(System.getProperty("user.dir")+"//BusinessETransfer_Automation//Test_Data//CBX_PreQueries.sql");
	ClearCellData.clearActualData();
	Fillo fill=new Fillo();
	Connection conn=fill.getConnection(System.getProperty("user.dir")+Constants_pack.Constants._ExcelPath);
	Recordset rs=conn.executeQuery("Select * from PAIN001 where RUN_CASE = 'Y'");
		 while (rs.next()) {
			GrpHdr GrpHdr =new GrpHdr();
			String TestCaseID = rs.getField("Test_ID");
			System.out.println(TestCaseID);
			
			if (rs.getField("SQL_QueryExecute").contains("SQL")) {
				DBConnector.DB_UpdateConnector(rs.getField("SQL_QueryExecute").substring(4,rs.getField("SQL_QueryExecute").length()));
			}
			
//			String Channel_ID = rs.getField("Channel_ID");
			
			Thread.sleep(5000);
//			System.out.println("PAIN001:"+TestCaseID);
			String _MsgId = null;
			if (rs.getField("Custom_Flag_Var").equalsIgnoreCase("D")) {
				_MsgId = SaltString.getSaltString(10);
				 GrpHdr.setMsgId(_MsgId);
				String _CreDtTm = rs.getField("CreDtTm_value");
//				String _CreDtTm = "2019-06-21T15:48:38";
				GrpHdr.setCreDtTm(_CreDtTm);
			}
			if (rs.getField("Custom_Flag_Var").equalsIgnoreCase("Y")) {
				_MsgId = SaltString.getSaltString(10);
				 GrpHdr.setMsgId(_MsgId);
				String _CreDtTm = GetSimpleDateFormat.getSimpleDateFormat();
//				String _CreDtTm = "2019-06-21T15:48:38";
				GrpHdr.setCreDtTm(_CreDtTm);
			}else{
				GrpHdrGson.getGrpHdrMsgId(rs, GrpHdr);
			}

			Update_ExcelResult.Update_DataReportAck(TestCaseID, _MsgId, "Message_ID");
			//NbOfTxs
			GrpHdrGson.GetgrpHdrsetNbOfTxs(rs, GrpHdr);
			//CtrlSum
			GrpHdrGson.GetsetCtrlSum(rs, GrpHdr);
			Authstn Authstn=new Authstn();
			AuthstnGson.GetAuthstn(rs, Authstn);
			//WIP Here Varun
			if (!rs.getField("Prtry_value").isEmpty()) {
				GrpHdr.setAuthstn(Authstn);
			}

			//InitgPty
			Othr Othr=new Othr();
			InitgPtyGson.InitgPtyOthrGson(rs, Othr);
			
			OrgId OrgId=new OrgId();
			OrgId.setOthr(Othr);
			
			Id Id=new Id();
			Id.setOrgId(OrgId);
			InitgPty InitgPty=new InitgPty();
			InitgPtyGson.InitgPtyIdGson(rs, InitgPty);
			if (!rs.getField("Id_value").isEmpty()) {
				InitgPty.setId(Id);
			}
			GrpHdr.setInitgPty(InitgPty);
	//****************************************************PmtInf****************************************************
			String TC_PmtInf_ID = rs.getField("TC_PmtInf_ID");
			Fillo fill3 = new Fillo();
			Connection conn3=fill3.getConnection(System.getProperty("user.dir")+Constants._ExcelPath);
			Recordset rs3=conn3.executeQuery("Select * from PMTINF where TC_PmtInf_ID = '"+TC_PmtInf_ID+"'");
			ArrayList<PmtInf> _addPmtInf = new ArrayList<PmtInf>();
			CstmrCdtTrfInitn CstmrCdtTrfInitn=null;
		while(rs3.next()){
//			System.out.println(""+rs3.getField("TC_PmtInf_ID"));
			CstmrCdtTrfInitn=new CstmrCdtTrfInitn();
			CstmrCdtTrfInitn.setGrpHdr(GrpHdr);
//			PmtTpInf Starts here
			PmtInf PmtInf =new PmtInf();
			PmtInfGsonkeyValue.PmtInfIdGson(rs3, PmtInf);
			
//			PmtTpInf Ends here
			SvcLvl SvcLvl=new SvcLvl();
			
		if (!rs3.getField("PmtTpInf_SvcPrty").isEmpty()) {
			PmTpInfSvcLvlGson.getSvcPrtry(rs3, SvcLvl);
		}	
		if (!rs3.getField("PmtTpInf_SvcLvl").isEmpty()) {
			PmTpInfSvcLvlGson.getSvcLvl(rs3, SvcLvl);
		}	
			LclInstrm LclInstrm=new LclInstrm();
			
		if (!rs3.getField("PmtTpInf_LclInstrm").isEmpty()) {
			PmTpInfSvcLvlGson.getLclInstrm(rs3, LclInstrm);
		}
		if (!rs3.getField("PmtTpInf_LclInstrmCd").isEmpty()) {
			PmTpInfSvcLvlGson.getLclInstrmCd(rs3, LclInstrm);
		}

		PmtTpInf PmtTpInf=new PmtTpInf();
		if (!rs3.getField("PmtTpInf_LclInstrm").isEmpty()) {	
			PmtTpInf.setLclInstrm(LclInstrm);
		}
		if (!rs3.getField("PmtTpInf_LclInstrmCd").isEmpty()) {	
			PmtTpInf.setLclInstrm(LclInstrm);
		}
		if (!rs3.getField("PmtTpInf_SvcPrty").isEmpty()) {	
			PmtTpInf.setSvcLvl(SvcLvl);
		}	
		if (!rs3.getField("PmtTpInf_SvcLvl").isEmpty()) {	
			PmtTpInf.setSvcLvl(SvcLvl);
		}	

		PmtInf.setPmtTpInf(PmtTpInf);
//		PmtTpInf Ends here

//			Dbtr Starts Here
			PstlAdr PstlAdr=new PstlAdr();
			//Varun WIP
			DbtrPstAdlGson.getDbtrPstlAdr(rs3, PstlAdr);

			ArrayList Address = new ArrayList();
			dbtrPstlAddress.getDbtrPstlAddress(rs3, Address);
			if (!Address.isEmpty()) {
				PstlAdr.setAdrLine(Address);
			}

			DbtrOthr DbtrOthr=new DbtrOthr();
			DbtrOrgIdGson.getDbtrOrgId(rs3, DbtrOthr);
			
			DbtrOrgId DbtrOrgId=new DbtrOrgId();
			DbtrOrgId.setOthr(DbtrOthr);
			
			//VarunUpdated
			getAnyBIC(rs3, DbtrOrgId);
			
			DbtrId  DbtrId=new DbtrId();
			DbtrId.setOrgId(DbtrOrgId);
			
			Dbtr Dbtr=new Dbtr();
			if (!rs3.getField("Id_value2").isEmpty()) {
				Dbtr.setId(DbtrId);
			}
			
			InitialPrtyNmGson.getInitgPtyNameGson(rs3, Dbtr);
			Dbtr.setPstlAdr(PstlAdr);
			
			PmtInf.setDbtr(Dbtr);
//			Dbtr Ends here

//			ChrgsAcct Starts here
			ChrgsAcctOthr Othr1=new ChrgsAcctOthr();

			ChrgsAcctId  Id1=new ChrgsAcctId();
			Id1.setOthr(Othr1);

			ChrgsAcct.ChrgsAcct ChrgsAcct = new ChrgsAcct.ChrgsAcct();
			//added varunToday
			if (!rs3.getField("Id_1value").isEmpty()){	
				SetChargesAccountId(rs3, Othr1);
				ChrgsAcct.setId(Id1);
			}

			if (!rs3.getField("ChargesAccount_ccy").isEmpty()) {
				ChrgsAcctGson.getChrgsAcctCcy(rs3, ChrgsAcct);
			}
		
		if (!rs3.getField("ChargesAccount_ccy").isEmpty() || !rs3.getField("Id_1value").isEmpty()) {
			PmtInf.setChrgsAcct(ChrgsAcct);
			if (!rs3.getField("ChargesAccount_ccy").isEmpty() && !rs3.getField("Id_1value").isEmpty()) {
				PmtInf.setChrgsAcct(ChrgsAcct);
			}
		}

//			DbtrAcct Starts here

			DbtrAcctOthr DbtrAcctOthr=new DbtrAcctOthr();
			DbtrAccGson.getDbtrAcct(rs3, DbtrAcctOthr);
			
			DbtrAcctId DbtrAcctId=new DbtrAcctId();
			DbtrAcctId.setOthr(DbtrAcctOthr);
			
			DbtrAcct DbtrAcct=new DbtrAcct();
//			Added below to hide debtor Account
			if (!rs3.getField("Id_2value").isEmpty()) {
				DbtrAcct.setId(DbtrAcctId);
			}
			DbtrAccGson.getDbtrAcctNm(rs3, DbtrAcct);
			
// 		Varun	
//		if (!rs3.getField("Id_2value").isEmpty()) {
			PmtInf.setDbtrAcct(DbtrAcct);
//		}

//			DbtrAgt Starts here
			DbtrAgtBrnchId BrnchId=new DbtrAgtBrnchId();
			DbtrAgtBrnchIdGson.getDbtrAgtBrnchId(rs3, BrnchId);

			DbtrAgtPstlAdr DbtrAgtPstlAdr=new DbtrAgtPstlAdr();
			getDbtrCtry(rs3, DbtrAgtPstlAdr);
			
			DbtrAgtOthr DbtrAgtOthr=new DbtrAgtOthr();
			DbtrAgtSetID(rs3, DbtrAgtOthr);
			
			DbtrAgtFinInstnId DbtrAgtFinInstnId=new DbtrAgtFinInstnId();
			DbtrAgtGson.getDbtrAgt(rs3, DbtrAgtFinInstnId);
			
			if (!rs3.getField("Ctry_value1").isEmpty()) {
				DbtrAgtFinInstnId.setPstlAdr(DbtrAgtPstlAdr);
			}
			
			if (!rs3.getField("Id_3value").isEmpty()) {
				DbtrAgtFinInstnId.setOthr(DbtrAgtOthr);
			}
			
			DbtrAgt DbtrAgt=new DbtrAgt();
			if (!rs3.getField("Id_3valueF").isEmpty()) {
				DbtrAgt.setBrnchId(BrnchId);
			}
			
			DbtrAgt.setFinInstnId(DbtrAgtFinInstnId);
			PmtInf.setDbtrAgt(DbtrAgt);
					
			System.setProperty("ROW", "2");
			Fillo fill1=new Fillo();
			Connection conn1=fill1.getConnection(System.getProperty("user.dir")+Constants._ExcelPath);
			Recordset rs1=conn1.executeQuery("Select * from SplmtryData where Splmtry_ID = '"+TC_PmtInf_ID+"'");
			ArrayList<SplmtryData> al =  new ArrayList<SplmtryData>();
			while(rs1.next()){
					SplmtryData SplmtryData1 =null;
					Envlp Envlp1=new Envlp();
//					Envlp1.setCnts(rs1.getField("Cnts_Value"));
					Cnts Cnts=new Cnts();
					Cnts.setPANID(rs1.getField("PANID_Value"));
					if(!rs1.getField("ChnlIndctr").isEmpty())
					{
					Cnts.setChnlIndctr(rs1.getField("ChnlIndctr"));
					}
					if(!rs1.getField("CustIpAddr").isEmpty())
					{
					Cnts.setCustIpAddr(rs1.getField("CustIpAddr"));
					}
					if(!rs1.getField("CustAuthMtd").isEmpty())
					{
					Cnts.setCustAuthMtd(rs1.getField("CustAuthMtd"));
					}
					if(!rs1.getField("AuthTp").isEmpty())
					{
					Cnts.setAuthTp(rs1.getField("AuthTp"));
					}
					if(!rs1.getField("FXId").isEmpty())
					{
					Cnts.setFXId(rs1.getField("FXId"));
					}
					if(!rs1.getField("PmtRlsDt").isEmpty())
					{
					Cnts.setPymtRelDt(rs1.getField("PmtRlsDt"));
					}
					/*Aishwarya commented code as part of new BTR Payload changes 
					//getChannelSeqID(rs1, Cnts);
					*/
					SplmtryData1=new SplmtryData();
					Envlp1.setCnts(Cnts);
					SplmtryData1.setEnvlp(Envlp1);
					al.add(SplmtryData1);
					CstmrCdtTrfInitn.setSplmtryData(al);
			}
			_addPmtInf.add(PmtInf);			
			CstmrCdtTrfInitn.setPmtInf1(_addPmtInf);
	//Ends PmtInf here		
			String CdtTrfTxInf_ID = rs3.getField("CdtTrfTxInf_ID");
			Fillo fill5 = new Fillo();
			Connection conn5=fill5.getConnection(System.getProperty("user.dir")+Constants._ExcelPath);
			Recordset rs5=conn5.executeQuery("Select * from CdtTrfTxInf where CdtTrfTxInf_ID = '"+CdtTrfTxInf_ID+"'");
			ArrayList<CdtTrfTxInf> _addCdtTrfTxInf=new ArrayList<CdtTrfTxInf>();
			while (rs5.next()) {
//			System.out.println(rs5.getField("CdtTrfTxInf_ID"));
				
//			CdtTrfTxInf starts here
			CdtTrfTxInf CdtTrfTxInf=new CdtTrfTxInf();
			
			//* ChrgBr
			GetKeyChrBr.getChrBrGson(rs5, CdtTrfTxInf);
			
			//* InstrForDbtrAgt
			getInstrForDbtrAgt(rs5, CdtTrfTxInf);
			
			//* PmtId
			PmtId PmtId=new PmtId();
			//WIP Varun
			
			PmtIdGson.getPmtID(rs5, PmtId);
			CdtTrfTxInf.setPmtId(PmtId);

			//Cdtr
			CdtrPstlAdr CdtrPstlAdr=new CdtrPstlAdr();
			CdtrGson.getCdtrPstlAdr(rs5, CdtrPstlAdr);
			
			ArrayList CdtrPstlAdr_Address=new ArrayList();
			cdtrPstlAddress.getCdtrPstlAddress(rs5, CdtrPstlAdr_Address);
			
			if (!CdtrPstlAdr_Address.isEmpty()) {
				CdtrPstlAdr.setAdrLine(CdtrPstlAdr_Address);
			}
			
			Cdtr Cdtr=new Cdtr();
			CdtrGson.getCdtrNm(rs5, Cdtr);
			Cdtr.setPstlAdr(CdtrPstlAdr);
			if(!rs5.getField("EmailAdr").isEmpty()||!rs5.getField("MobNb").isEmpty()||!rs5.getField("Othr").isEmpty())
			{
			CtctDtls CtctDtls=new CtctDtls();
			if(!rs5.getField("EmailAdr").isEmpty())
			{	
			CtctDtls.setEmailAdr(rs5.getField("EmailAdr"));
			}
			if(!rs5.getField("MobNb").isEmpty())
			{	
			CtctDtls.setMobno(rs5.getField("MobNb"));
			}
			if(!rs5.getField("Othr").isEmpty())
			{	
			CtctDtls.setOthr(rs5.getField("Othr"));
			}


			Cdtr.setCtctDtls(CtctDtls);
			}
			CdtTrfTxInf.setCdtr(Cdtr);
			
			//Amt
			EqvtAmt EqvtAmt=new EqvtAmt();
			EqvtAmtGson.getEqvtAmt(rs5, EqvtAmt);
			
			InstdAmt InstdAmt=new InstdAmt();
			InstdAmtGson.getInstdAmt(rs5, InstdAmt);
			
			Amt Amt = new Amt();
			if ((!rs5.getField("Eqt_Amt").isEmpty()) || (!rs5.getField("Eqt_AmtCcyTransfer").isEmpty())  || (!rs5.getField("Eqt_AmtCcy").isEmpty()) ) {
				Amt.setEqvtAmt(EqvtAmt);
				if ((!rs5.getField("Eqt_Amt").isEmpty()) && (!rs5.getField("Eqt_AmtCcyTransfer").isEmpty())  && (!rs5.getField("Eqt_AmtCcy").isEmpty())  ) {
					Amt.setEqvtAmt(EqvtAmt);
				}
			}

			if ((!rs5.getField("Ccy_value").isEmpty()) || (!rs5.getField("Amt_value").isEmpty())) {
				Amt.setInstdAmt(InstdAmt);
				if ((!rs5.getField("Ccy_value").isEmpty()) && (!rs5.getField("Amt_value").isEmpty())) {
					Amt.setInstdAmt(InstdAmt);
				}
			}
			CdtTrfTxInf.setAmt(Amt);
//			Amt ends here
			
//			XchgRateInf starts here

			XchgRateInf XchgRateInf=new XchgRateInf();
			XchgRateInfGson.getXchgRateInf(rs5, XchgRateInf);

			if ((!rs5.getField("XchgRate_value").isEmpty()) || (!rs5.getField("RateTp_value").isEmpty())  || (!rs5.getField("CtrctId_value").isEmpty())) {
					CdtTrfTxInf.setXchgRateInf(XchgRateInf);
				if ((!rs5.getField("XchgRate_value").isEmpty()) && (!rs5.getField("RateTp_value").isEmpty())  && (!rs5.getField("CtrctId_value").isEmpty())) {
					CdtTrfTxInf.setXchgRateInf(XchgRateInf);
				}
			}
			
//			XchgRateInf Ends here		
			IntrmyAgt1FinInstnId IntrmyAgt1FinInstnId =new IntrmyAgt1FinInstnId();
			IntrmyAgtGson.getIntrmyAgt1(rs5, IntrmyAgt1FinInstnId);
			
			IntrmyAgt1 IntrmyAgt1=new IntrmyAgt1();
			IntrmyAgt1.setFinInstnId(IntrmyAgt1FinInstnId);
			
			if(!rs5.getField("BICFI_val").isEmpty()){
				CdtTrfTxInf.setIntrmyAgt1(IntrmyAgt1);
			}

//		CdtrAgt Starts here
			ClrSysId ClrSysId=new ClrSysId();
			ClrSysIdGson.getClrSysId(rs5, ClrSysId);
			
			ClrSysMmbId ClrSysMmbId=new ClrSysMmbId();
			ClrSysMmbIdGson.getClrSysMmbId(rs5, ClrSysMmbId);
		
			if (!rs5.getField("Cd_ClrSysId_value").isEmpty()) {
				ClrSysId.setCd(rs5.getField("Cd_ClrSysId_value"));
			}
			ClrSysMmbId.setClrSysId(ClrSysId);
			
			if (!rs5.getField("Prtry_ClrSysId_value").isEmpty()) {
				ClrSysMmbId.setClrSysId(ClrSysId);
			}
			
			FinInstnId FinInstnId=new FinInstnId();
		 {
				FinInstnId.setClrSysMmbId(ClrSysMmbId);
			}
			
			FinInstIdGson.getFinInstnIdSetNm(rs5, FinInstnId);
			
			CdtrAgtPstlAdr CdtrAgtPstlAdr=new CdtrAgtPstlAdr();
			cdtrAgtpstladdress.getCtrAgtPstAdr(rs5, CdtrAgtPstlAdr);

			ArrayList AddressCdtrAgt = new ArrayList();
			cdtrAgtpstladdress.getCdtrAgtPstlAdrAddress(rs5, AddressCdtrAgt);
			if (!AddressCdtrAgt.isEmpty()) {
				CdtrAgtPstlAdr.setAdrLine(AddressCdtrAgt);
			}	

			FinInstnId.setPstlAdr(CdtrAgtPstlAdr);
			getBICFI(rs5, FinInstnId);

			CdtrAgt CdtrAgt=new CdtrAgt();
			//CdtrAgt.setFinInstnId(FinInstnId);
			//ASH COMMENTED FOR CDTRAGT
			//CdtTrfTxInf.setCdtrAgt(CdtrAgt);

			//CdtrAcct
			//added varun 29.8.2019
			if(!rs5.getField("Id_value1").isEmpty()) {
				CdtrAcctOthr CdtrAcctOthr=new CdtrAcctOthr();
				CdtrAcctGson.getCdtrAccIdNm(rs5, CdtrAcctOthr);
				
				CdtrAcctId  CdtrAcctId=new CdtrAcctId();
				CdtrAcctId.setOthr(CdtrAcctOthr);
				
				CdtrAcct CdtrAcct=new CdtrAcct();
				CdtrAcct.setId(CdtrAcctId);
				CdtTrfTxInf.setCdtrAcct(CdtrAcct);
			}
			
			if(!rs5.getField("IBAN").isEmpty()) {
				
				CdtrAcctId  CdtrAcctId=new CdtrAcctId();
				getIBAN(rs5, CdtrAcctId);
				
				CdtrAcct CdtrAcct=new CdtrAcct();
				CdtrAcct.setId(CdtrAcctId);
				CdtTrfTxInf.setCdtrAcct(CdtrAcct);
			}
			
//		  	CdtrAgt Ends here		
//	        RmtInf Starts here
			RmtInf RmtInf=new RmtInf();
			RmtInfGson.getRmtInf(rs5, RmtInf); //Varun WIP
			if (!RmtInf.getUstrd().isEmpty()){
					CdtTrfTxInf.setRmtInf(RmtInf);
			}
//		     RmtInf Ends here
//			SplmtryData Starts here
			ArrayList<SplmtryData> SplmtryData=new ArrayList<SplmtryData>();
			
			//One
			Envlp Envlp1=new Envlp();
//			SplmntryCntsGson.getSplmtryData_CntsOne(rs5, Envlp1);
			SplmtryData SplmtryData1=new SplmtryData();
			Cnts Cnts=new Cnts();
			
			AutoDepSplmntryData(rs5, Cnts);

			SplmtryData1.setEnvlp(Envlp1);
			Envlp1.setCnts(Cnts);
			if (!rs5.getField("SplmtryData_Cnts").isEmpty()) {
				SplmtryData.add(SplmtryData1);
			}
			CdtTrfTxInf.setSplmtryData1(SplmtryData);
			
//			SplmtryData Ends here
//			Purp
			Purp Purp=new Purp();
			getPurp(rs5, Purp);
			if (!rs5.getField("Prtry_value").isEmpty()){
				CdtTrfTxInf.setPurp(Purp);
			}

//			CdtTrfTxInf CdtTrfTxInf1[]={CdtTrfTxInf};
//			PmtInf.setCdtTrfTxInf(CdtTrfTxInf1);
			_addCdtTrfTxInf.add(CdtTrfTxInf);
			PmtInf.setCdtTrfTxInf1(_addCdtTrfTxInf);
		}	
	}
		
	//CdtTrfTxInf Ends here here
			Document Document=new Document();
			Document.setCstmrCdtTrfInitn(CstmrCdtTrfInitn);

			RootClass RootClass=new RootClass();
			RootClass.setDocument(Document);

			Gson Gson =new GsonBuilder().setPrettyPrinting().create();
			JsonElement str=Gson.toJsonTree(RootClass);
			String prettyJsonString = Gson.toJson(str);

		     FileWriter fw=new FileWriter(System.getProperty("user.dir")+"\\BusinessETransfer_Automation\\_Output\\"+TestCaseID+"_Pain001_REQ.json");    
	           fw.write(prettyJsonString);    
	         fw.close();    

		    POST_requestApi.ExecutePOST_API(TestCaseID,_API_Link,channelId,service_type,flow_id);
		    
		    Fillo fill1=new Fillo();
			Connection conn1=fill1.getConnection(System.getProperty("user.dir")+Constants_pack.Constants._ExcelPath);
			Recordset rs1=conn1.executeQuery("Select * from PAIN001 where TEST_ID = '"+TestCaseID+"'");
			while (rs1.next()) {
				if (rs1.getField("Cancellation_Case").equalsIgnoreCase("Y") && rs1.getField("RUN_CASE").equalsIgnoreCase("Y")) {
//					if (TxnProcOpnValidation.RecusriveStatusCheck(rs1.getField("Message_ID"), rs1.getField("Payment_Status"))==true) {
						GSON_CACNDataMainipulation.Cancellation(TestCaseID);
						POST_requestApi.ExecuteCancellationAPI(TestCaseID,CACN_API_URL,channelId,service_type,flow_id);
						Thread.sleep(5000);
						Update_ExcelResult.Update_DataReportAck(TestCaseID, TxnProcOpnValidation.getStatusForCACN(rs1.getField("Message_ID")), "CANCEL_STATUS");
//					}
				}
			}

/*		 	
 * 			Fillo fill1=new Fillo();
			Connection conn1=fill1.getConnection(Constants_pack.Constants._ExcelPath);
			Recordset rs1=conn1.executeQuery("Select * from PAIN001 where TEST_ID = '"+TestCaseID+"'");
			while (rs1.next()) {
				
		     if (rs1.getField("Selenium_UIActions").equals("Y") && rs1.getField("SCENARIO").equalsIgnoreCase("PUSH_IWSMT199")) {
		    	 System.out.println("MT File Execution in Progress..");
		    	 Thread.sleep(50000);
		    	 if (GetValueMT103.getdata_TXN_STATUS(rs1.getField("File_WorkitemID")).equalsIgnoreCase("Awaiting IWS Acknowledgement")) {
		    		 System.out.println(rs.getField("File_WorkitemID"));
			    	 TestMT103UI.PUSHMT103(rs1.getField("File_WorkitemID"));
			    	 Thread.sleep(10000);
		    	 }

				 if (GetValueMT103.getdata_TXN_STATUS(rs1.getField("File_WorkitemID")).equalsIgnoreCase("Awaiting IWS Status")) {
					 MT199.CreateFile_MT199(rs1.getField("File_WorkitemID"));	
					 TestMT199UI.PUSHMT199();
				}
		     }

		     if (rs1.getField("Selenium_UIActions").equals("Y") && rs1.getField("SCENARIO").equalsIgnoreCase("PUSH_IWSMT103_ACK")) {
		    	 System.out.println("MT File Execution in Progress..");
		    	 Thread.sleep(50000);
		    	 if (GetValueMT103.getdata_TXN_STATUS(rs1.getField("File_WorkitemID")).equalsIgnoreCase("Awaiting IWS Acknowledgement")) {
		    		 System.out.println(rs1.getField("File_WorkitemID"));
			    	 TestMT103UI.PUSHMT103(rs1.getField("File_WorkitemID"));
			    	 Thread.sleep(10000);
		    	 }
		     }
		     
		     if (rs1.getField("Selenium_UIActions").equals("Y") && rs1.getField("SCENARIO").equalsIgnoreCase("PUSH_IWSMT900")) {
		    	 System.out.println("MT File Execution in Progress..");
		    	 Thread.sleep(50000);
		    	 if (GetValueMT103.getdata_TXN_STATUS(rs1.getField("File_WorkitemID")).equalsIgnoreCase("Awaiting IWS Acknowledgement")) {
		    		 System.out.println(rs1.getField("File_WorkitemID"));
			    	 TestMT103UI.PUSHMT103(rs1.getField("File_WorkitemID"));
			    	 Thread.sleep(10000);
		    	 }
		    	 
				 if (GetValueMT103.getdata_TXN_STATUS(rs1.getField("File_WorkitemID")).equalsIgnoreCase("Awaiting IWS Status")) {
					 MT199.CreateFile_MT199(rs1.getField("File_WorkitemID"));	
					 TestMT199UI.PUSHMT199();
					 Thread.sleep(10000);
				}
				 
				 if (GetValueMT103.getdata_TXN_STATUS(rs1.getField("File_WorkitemID")).equalsIgnoreCase("Payment Completed")) {
					 MT900.CreateFile_MT900(rs1.getField("File_WorkitemID"));	
					 TestMT900UI.PUSHMT900();
					 Thread.sleep(10000);
				}
		     }
		     
		     if (rs1.getField("Selenium_UIActions").equals("Y") && rs1.getField("SCENARIO").equalsIgnoreCase("PUSH_ACK_MT199")) {
		    	 System.out.println("MT File Execution in Progress..");
		    	 
				 if (GetValueMT103.getdata_TXN_STATUS(rs1.getField("File_WorkitemID")).equalsIgnoreCase("Awaiting IWS Acknowledgement")) {
					 MT199.CreateFile_MT199(rs1.getField("File_WorkitemID"));	
					 TestMT199UI.PUSHMT199();
					 Thread.sleep(10000);
				 }
		     }

		     
		     if (rs1.getField("Selenium_UIActions").equals("Y") && rs1.getField("SCENARIO").equalsIgnoreCase("PUSH_ACK_MT199_MT900")) {
		    	 System.out.println("MT File Execution in Progress..");
		    	 
				 if (GetValueMT103.getdata_TXN_STATUS(rs1.getField("File_WorkitemID")).equalsIgnoreCase("Awaiting IWS Acknowledgement")) {
					 TestMT103UI.PUSHMT103(rs1.getField("File_WorkitemID"));					 
					 Thread.sleep(10000);
				 }
				 
				 if (GetValueMT103.getdata_TXN_STATUS(rs1.getField("File_WorkitemID")).equalsIgnoreCase("Awaiting IWS Status")) {
					 MT199.CreateFile_MT199(rs1.getField("File_WorkitemID"));	
					 TestMT199UI.PUSHMT199();
					 Thread.sleep(10000);
				}
				 
				 if (GetValueMT103.getdata_TXN_STATUS(rs1.getField("File_WorkitemID")).equalsIgnoreCase("Payment Completed")) {
					 MT900.CreateFile_MT900(rs1.getField("File_WorkitemID"));	
					 TestMT900UI.PUSHMT900();
					 Thread.sleep(10000);
				}
				 
		     }
		     
		     if (rs1.getField("Selenium_UIActions").equals("Y") && rs1.getField("SCENARIO").equalsIgnoreCase("PUSH_IWSMT199")) {
		    	 System.out.println("MT File Execution in Progress..");
		    	 Thread.sleep(50000);
		    	 if (GetValueMT103.getdata_TXN_STATUS(rs1.getField("File_WorkitemID")).equalsIgnoreCase("Awaiting IWS Acknowledgement")) {
		    		 System.out.println(rs1.getField("File_WorkitemID"));
			    	 TestMT103UI.PUSHMT103(rs1.getField("File_WorkitemID"));
			    	 Thread.sleep(10000);
		    	 }
		    	 
				 if (GetValueMT103.getdata_TXN_STATUS(rs1.getField("File_WorkitemID")).equalsIgnoreCase("Awaiting IWS Status")) {
					 MT199.CreateFile_MT199(rs1.getField("File_WorkitemID"));	
					 TestMT199UI.PUSHMT199();
				}
		     }
		     
		     if (rs1.getField("Selenium_UIActions").equals("Y") && rs1.getField("SCENARIO").equalsIgnoreCase("PerformManual_Actions")) {
		    	 System.out.println("Maker Checker Action Executes :)");
		    	 Thread.sleep(400000);
		    	 WebDriver driver = null;
		    	 if (GetValueMT103.getdata_TXN_STATUS(rs1.getField("File_WorkitemID")).equalsIgnoreCase("Payment Suspended")) {
		    		 UI_CC2_ManualActionMakerChecker.CC2_LinearScript.Execute_FGT(driver,"BCCMKR1",rs1.getField("PaymentWorkItem_ID"));
			    	 Thread.sleep(10000);
		    	 }

				 if (GetValueMT103.getdata_TXN_STATUS(rs1.getField("File_WorkitemID")).equalsIgnoreCase("Awaiting Suspense Approval")) {
					 UI_CC2_ManualActionMakerChecker.CC2_LinearScript.Execute_FGT(driver,"BCCCKR1",rs1.getField("PaymentWorkItem_ID"));	
					 Thread.sleep(10000);
				}
		     } 
			}*/
	   }
	 }catch (Exception e){
	 	 System.out.println("No Records Found for PAIN001 !!");
	 	 e.printStackTrace();
	 }
//		 MTFileUIAction.PUSHMTFiles();
//		 Execute_QueriesTestToolFileName.Get_SQLQueriesTestToolFile();
//		 FileCopy.CopyTestDataExcel();
//		 ZipFiles.WinZip_JSONFiles();
}

public static void getChannelSeqID(Recordset rs, Cnts Cnts) throws FilloException {
	if (!rs.getField("ChannelSeqId").isEmpty() && !rs.getField("ChannelSeqId").equalsIgnoreCase("M") && !rs.getField("ChannelSeqId").equalsIgnoreCase("I") && !rs.getField("ChannelSeqId").contains("#I") && !rs.getField("ChannelSeqId").contains("#S")) {
		Cnts.setChannelSeqId(rs.getField("ChannelSeqId"));
	}else if(rs.getField("ChannelSeqId").equalsIgnoreCase("M")) {
		Cnts.setChannelSeqId("");
	}else if(rs.getField("ChannelSeqId").isEmpty()) {
		
	}else if(rs.getField("ChannelSeqId").contains("#I")){
		Cnts.setChannelSeqId(rs.getField("ChannelSeqId").substring(2,rs.getField("ChannelSeqId").length()));
	}else if(rs.getField("ChannelSeqId").contains("#S")){
		Cnts.setChannelSeqId(rs.getField("ChannelSeqId").substring(2,rs.getField("ChannelSeqId").length()));
		Cnts.setChannelSeqId(rs.getField("ChannelSeqId").substring(2,rs.getField("ChannelSeqId").length()));
	}
}

public static void AutoDepSplmntryData(Recordset rs, Cnts Cnts)	throws Exception {
	/*if (!rs.getField("AutoDep").isEmpty() && !rs.getField("AutoDep").equalsIgnoreCase("M") && !rs.getField("AutoDep").equalsIgnoreCase("I") && !rs.getField("AutoDep").contains("#I") && !rs.getField("AutoDep").contains("#S")) {
		Cnts.setAutoDep(rs.getField("AutoDep"));
	}else if(rs.getField("AutoDep").equalsIgnoreCase("M")) {
		Cnts.setAutoDep("");
	}else if(rs.getField("AutoDep").isEmpty()) {
		
	}else if(rs.getField("AutoDep").contains("#I")){
		Cnts.setAutoDep(rs.getField("AutoDep").substring(2,rs.getField("AutoDep").length()));
	}else if(rs.getField("AutoDep").contains("#S")){
		Cnts.setAutoDep(rs.getField("AutoDep").substring(2,rs.getField("AutoDep").length()));
		Cnts.setAutoDep(rs.getField("AutoDep").substring(2,rs.getField("AutoDep").length()));
	}
	*/
//	Cnts.setStlmMth(rs.getField("StlmMth"));
	if (!rs.getField("StlmMth").isEmpty() && !rs.getField("StlmMth").equalsIgnoreCase("M") && !rs.getField("StlmMth").equalsIgnoreCase("I") && !rs.getField("StlmMth").contains("#I") && !rs.getField("StlmMth").contains("#S")) {
		Cnts.setStlmMth(rs.getField("StlmMth"));
	}else if(rs.getField("StlmMth").equalsIgnoreCase("M")) {
		Cnts.setStlmMth("");
	}else if(rs.getField("StlmMth").isEmpty()) {
		
	}else if(rs.getField("StlmMth").contains("#I")){
		Cnts.setStlmMth(rs.getField("StlmMth").substring(2,rs.getField("StlmMth").length()));
	}else if(rs.getField("StlmMth").contains("#S")){
		Cnts.setStlmMth(rs.getField("StlmMth").substring(2,rs.getField("StlmMth").length()));
		Cnts.setStlmMth(rs.getField("StlmMth").substring(2,rs.getField("StlmMth").length()));
	}
	
	
//	Cnts.setPymtAmt(rs.getField("PymtAmt"));
	if (!rs.getField("PymtAmt").isEmpty() && !rs.getField("PymtAmt").equalsIgnoreCase("M") && !rs.getField("PymtAmt").equalsIgnoreCase("I") && !rs.getField("PymtAmt").contains("#I") && !rs.getField("PymtAmt").contains("#S")) {
		Cnts.setPymtAmt(rs.getField("PymtAmt"));
	}else if(rs.getField("PymtAmt").equalsIgnoreCase("M")) {
		Cnts.setPymtAmt("");
	}else if(rs.getField("PymtAmt").isEmpty()) {
		
	}else if(rs.getField("PymtAmt").contains("#I")){
		Cnts.setPymtAmt(rs.getField("PymtAmt").substring(2,rs.getField("PymtAmt").length()));
	}else if(rs.getField("PymtAmt").contains("#S")){
		Cnts.setPymtAmt(rs.getField("PymtAmt").substring(2,rs.getField("PymtAmt").length()));
		Cnts.setPymtAmt(rs.getField("PymtAmt").substring(2,rs.getField("PymtAmt").length()));
	}
//AISWARYA ADDED DBAMT
	if (!rs.getField("DbAmt").isEmpty() && !rs.getField("DbAmt").equalsIgnoreCase("M") && !rs.getField("DbAmt").equalsIgnoreCase("I") && !rs.getField("DbAmt").contains("#I") && !rs.getField("DbAmt").contains("#S")) {
		Cnts.setDbAmt(rs.getField("DbAmt"));
	}else if(rs.getField("DbAmt").equalsIgnoreCase("M")) {
		Cnts.setDbAmt("");
	}else if(rs.getField("DbAmt").isEmpty()) {
		
	}else if(rs.getField("DbAmt").contains("#I")){
		Cnts.setDbAmt(rs.getField("DbAmt").substring(2,rs.getField("DbAmt").length()));
	}else if(rs.getField("DbAmt").contains("#S")){
		Cnts.setDbAmt(rs.getField("DbAmt").substring(2,rs.getField("DbAmt").length()));
		Cnts.setDbAmt(rs.getField("DbAmt").substring(2,rs.getField("DbAmt").length()));
	}

//	Cnts.setRTPFlflmntInd(rs.getField("RTPFlflmntInd"));ash commented 
/*	if (!rs.getField("RTPFlflmntInd").isEmpty() && !rs.getField("RTPFlflmntInd").equalsIgnoreCase("M") && !rs.getField("RTPFlflmntInd").equalsIgnoreCase("I") && !rs.getField("RTPFlflmntInd").contains("#I") && !rs.getField("RTPFlflmntInd").contains("#S")) {
		Cnts.setRTPFlflmntInd(rs.getField("RTPFlflmntInd"));
	}else if(rs.getField("RTPFlflmntInd").equalsIgnoreCase("M")) {
		Cnts.setRTPFlflmntInd("");
	}else if(rs.getField("RTPFlflmntInd").isEmpty()) {
		
	}else if(rs.getField("RTPFlflmntInd").contains("#I")){
		Cnts.setRTPFlflmntInd(rs.getField("RTPFlflmntInd").substring(2,rs.getField("RTPFlflmntInd").length()));
	}else if(rs.getField("RTPFlflmntInd").contains("#S")){
		Cnts.setRTPFlflmntInd(rs.getField("RTPFlflmntInd").substring(2,rs.getField("RTPFlflmntInd").length()));
		Cnts.setRTPFlflmntInd(rs.getField("RTPFlflmntInd").substring(2,rs.getField("RTPFlflmntInd").length()));
	}
	*/
	//AISWARYA ADDED CCY
	if (!rs.getField("Ccy").isEmpty() && !rs.getField("Ccy").equalsIgnoreCase("M") && !rs.getField("Ccy").equalsIgnoreCase("I") && !rs.getField("Ccy").contains("#I") && !rs.getField("Ccy").contains("#S")) {
		Cnts.setCcy(rs.getField("Ccy"));
	}else if(rs.getField("Ccy").equalsIgnoreCase("M")) {
		Cnts.setCcy("");
	}else if(rs.getField("Ccy").isEmpty()) {
		
	}else if(rs.getField("Ccy").contains("#I")){
		Cnts.setCcy(rs.getField("Ccy").substring(2,rs.getField("Ccy").length()));
	}else if(rs.getField("Ccy").contains("#S")){
		Cnts.setCcy(rs.getField("Ccy").substring(2,rs.getField("Ccy").length()));
		Cnts.setCcy(rs.getField("Ccy").substring(2,rs.getField("Ccy").length()));
	}
	
	if (!rs.getField("ProductCd").isEmpty() && !rs.getField("ProductCd").equalsIgnoreCase("M") && !rs.getField("ProductCd").equalsIgnoreCase("I") && !rs.getField("ProductCd").contains("#I") && !rs.getField("ProductCd").contains("#S")) {
		Cnts.setProductCd(rs.getField("ProductCd"));
	}else if(rs.getField("ProductCd").equalsIgnoreCase("M")) {
		Cnts.setProductCd("");
	}else if(rs.getField("ProductCd").isEmpty()) {
		
	}else if(rs.getField("ProductCd").contains("#I")){
		Cnts.setProductCd(rs.getField("ProductCd").substring(2,rs.getField("ProductCd").length()));
	}else if(rs.getField("ProductCd").contains("#S")){
		Cnts.setProductCd(rs.getField("ProductCd").substring(2,rs.getField("ProductCd").length()));
		Cnts.setProductCd(rs.getField("ProductCd").substring(2,rs.getField("ProductCd").length()));
	}
	
	if (!rs.getField("DbtrPuId").isEmpty() && !rs.getField("DbtrPuId").equalsIgnoreCase("M") && !rs.getField("DbtrPuId").equalsIgnoreCase("I") && !rs.getField("DbtrPuId").contains("#I") && !rs.getField("DbtrPuId").contains("#S")) {
		Cnts.setDbtrPuId(rs.getField("DbtrPuId"));
	}else if(rs.getField("DbtrPuId").equalsIgnoreCase("M")) {
		Cnts.setDbtrPuId("");
	}else if(rs.getField("DbtrPuId").isEmpty()) {
		
	}else if(rs.getField("DbtrPuId").contains("#I")){
		Cnts.setDbtrPuId(rs.getField("DbtrPuId").substring(2,rs.getField("DbtrPuId").length()));
	}else if(rs.getField("DbtrPuId").contains("#S")){
		Cnts.setDbtrPuId(rs.getField("DbtrPuId").substring(2,rs.getField("DbtrPuId").length()));
		Cnts.setDbtrPuId(rs.getField("DbtrPuId").substring(2,rs.getField("DbtrPuId").length()));
	}
	
	if (!rs.getField("PymtExpDt").isEmpty() && !rs.getField("PymtExpDt").equalsIgnoreCase("M") && !rs.getField("PymtExpDt").equalsIgnoreCase("I") && !rs.getField("PymtExpDt").contains("#I") && !rs.getField("PymtExpDt").contains("#S")) {
		Cnts.setPymtExpDt(rs.getField("PymtExpDt"));
	}else if(rs.getField("PymtExpDt").equalsIgnoreCase("M")) {
		Cnts.setPymtExpDt("");
	}else if(rs.getField("PymtExpDt").isEmpty()) {
		
	}else if(rs.getField("PymtExpDt").contains("#I")){
		Cnts.setPymtExpDt(rs.getField("PymtExpDt").substring(2,rs.getField("PymtExpDt").length()));
	}else if(rs.getField("PymtExpDt").contains("#S")){
		Cnts.setPymtExpDt(rs.getField("PymtExpDt").substring(2,rs.getField("PymtExpDt").length()));
		Cnts.setPymtExpDt(rs.getField("PymtExpDt").substring(2,rs.getField("PymtExpDt").length()));
	}
	
/*	if (!rs.getField("FXId").isEmpty() && !rs.getField("FXId").equalsIgnoreCase("M") && !rs.getField("FXId").equalsIgnoreCase("I") && !rs.getField("FXId").contains("#I") && !rs.getField("FXId").contains("#S")) {
		Cnts.setFXId(rs.getField("FXId"));
	}else if(rs.getField("FXId").equalsIgnoreCase("M")) {
		Cnts.setFXId("");
	}else if(rs.getField("FXId").isEmpty()) {
		
	}else if(rs.getField("FXId").contains("#I")){
		Cnts.setFXId(rs.getField("FXId").substring(2,rs.getField("FXId").length()));
	}else if(rs.getField("FXId").contains("#S")){
		Cnts.setFXId(rs.getField("FXId").substring(2,rs.getField("FXId").length()));
		Cnts.setFXId(rs.getField("FXId").substring(2,rs.getField("FXId").length()));
	}
	
	if (!rs.getField("PmtRlsDt").isEmpty()) {
		GetSimpleDateFormat g1=new GetSimpleDateFormat();
		String PymtRElDt=g1.getSimpleDateFormat1();
		Cnts.setPymtRelDt(PymtRElDt);
	}
	*/
	if (!rs.getField("ChnlPymtId").isEmpty() && !rs.getField("ChnlPymtId").equalsIgnoreCase("M") && !rs.getField("ChnlPymtId").equalsIgnoreCase("I") && !rs.getField("ChnlPymtId").contains("#I") && !rs.getField("ChnlPymtId").contains("#S")) {
//		PmtId.setEndToEndId(rs.getField("EndToEndId_value"));
		if (rs.getField("ChnlPymtId").contains("#E2E")) {
			String EndtoEndId  = rs.getField("ChnlPymtId").substring(2,rs.getField("ChnlPymtId").length());
			Cnts.setChnlPymtId(EndtoEndId);
		}else{
			Cnts.setChnlPymtId(SaltString.getRandkeys(10));
		}
	}else if(rs.getField("ChnlPymtId").equalsIgnoreCase("M")) {
		Cnts.setChnlPymtId("");
	}else if(rs.getField("ChnlPymtId").isEmpty()) {
		
	}else if(rs.getField("ChnlPymtId").contains("#I")){
		Cnts.setChnlPymtId(rs.getField("ChnlPymtId").substring(2,rs.getField("ChnlPymtId").length()));
	}else if(rs.getField("ChnlPymtId").contains("#S")){
		Cnts.setChnlPymtId(rs.getField("ChnlPymtId").substring(2,rs.getField("ChnlPymtId").length()));
		Cnts.setChnlPymtId(rs.getField("ChnlPymtId").substring(2,rs.getField("ChnlPymtId").length()));
	}
//	Cnts.setSecurityQuestion(rs5.getField("SecurityQuestion"));
	
	if (!rs.getField("SecurityQuestion").isEmpty() && !rs.getField("SecurityQuestion").equalsIgnoreCase("M") && !rs.getField("SecurityQuestion").equalsIgnoreCase("I") && !rs.getField("SecurityQuestion").contains("#I") && !rs.getField("SecurityQuestion").contains("#S")) {
		Cnts.setSecurityQuestion(rs.getField("SecurityQuestion"));
	}else if(rs.getField("SecurityQuestion").equalsIgnoreCase("M")) {
		Cnts.setSecurityQuestion("");
	}else if(rs.getField("SecurityQuestion").isEmpty()) {
		
	}else if(rs.getField("SecurityQuestion").contains("#I")){
		Cnts.setSecurityQuestion(rs.getField("SecurityQuestion").substring(2,rs.getField("SecurityQuestion").length()));
	}else if(rs.getField("SecurityQuestion").contains("#S")){
		Cnts.setSecurityQuestion(rs.getField("SecurityQuestion").substring(2,rs.getField("SecurityQuestion").length()));
		Cnts.setSecurityQuestion(rs.getField("SecurityQuestion").substring(2,rs.getField("SecurityQuestion").length()));
	}
	
//	Cnts.setSecurityAnswer(rs5.getField("SecurityAnswer"));
	
	if (!rs.getField("SecurityAnswer").isEmpty() && !rs.getField("SecurityAnswer").equalsIgnoreCase("M") && !rs.getField("SecurityAnswer").equalsIgnoreCase("I") && !rs.getField("SecurityAnswer").contains("#I") && !rs.getField("SecurityAnswer").contains("#S")) {
		Cnts.setSecurityAnswer(rs.getField("SecurityAnswer"));
	}else if(rs.getField("SecurityAnswer").equalsIgnoreCase("M")) {
		Cnts.setSecurityAnswer("");
	}else if(rs.getField("SecurityAnswer").isEmpty()) {
		
	}else if(rs.getField("SecurityAnswer").contains("#I")){
		Cnts.setSecurityAnswer(rs.getField("SecurityAnswer").substring(2,rs.getField("SecurityAnswer").length()));
	}else if(rs.getField("SecurityAnswer").contains("#S")){
		Cnts.setSecurityAnswer(rs.getField("SecurityAnswer").substring(2,rs.getField("SecurityAnswer").length()));
		Cnts.setSecurityAnswer(rs.getField("SecurityAnswer").substring(2,rs.getField("SecurityAnswer").length()));
	}
	
	
//	Cnts.setHashType(rs5.getField("HashType"));
	
	if (!rs.getField("HashType").isEmpty() && !rs.getField("HashType").equalsIgnoreCase("M") && !rs.getField("HashType").equalsIgnoreCase("I") && !rs.getField("HashType").contains("#I") && !rs.getField("HashType").contains("#S")) {
		Cnts.setHashType(rs.getField("HashType"));
	}else if(rs.getField("HashType").equalsIgnoreCase("M")) {
		Cnts.setHashType("");
	}else if(rs.getField("HashType").isEmpty()) {
		
	}else if(rs.getField("HashType").contains("#I")){
		Cnts.setHashType(rs.getField("HashType").substring(2,rs.getField("HashType").length()));
	}else if(rs.getField("HashType").contains("#S")){
		Cnts.setHashType(rs.getField("HashType").substring(2,rs.getField("HashType").length()));
		Cnts.setHashType(rs.getField("HashType").substring(2,rs.getField("HashType").length()));
	}
	
	
//	Cnts.setHashSalt(rs5.getField("HashSalt"));
	
	if (!rs.getField("HashSalt").isEmpty() && !rs.getField("HashSalt").equalsIgnoreCase("M") && !rs.getField("HashSalt").equalsIgnoreCase("I") && !rs.getField("HashSalt").contains("#I") && !rs.getField("HashSalt").contains("#S")) {
		Cnts.setHashSalt(rs.getField("HashSalt"));
	}else if(rs.getField("HashSalt").equalsIgnoreCase("M")) {
		Cnts.setHashSalt("");
	}else if(rs.getField("HashSalt").isEmpty()) {
		
	}else if(rs.getField("HashSalt").contains("#I")){
		Cnts.setHashSalt(rs.getField("HashSalt").substring(2,rs.getField("HashSalt").length()));
	}else if(rs.getField("HashSalt").contains("#S")){
		Cnts.setHashSalt(rs.getField("HashSalt").substring(2,rs.getField("HashSalt").length()));
		Cnts.setHashSalt(rs.getField("HashSalt").substring(2,rs.getField("HashSalt").length()));
	}
	
	if (!rs.getField("AcctCretDt").isEmpty() && !rs.getField("AcctCretDt").equalsIgnoreCase("M") && !rs.getField("AcctCretDt").equalsIgnoreCase("I") && !rs.getField("AcctCretDt").contains("#I") && !rs.getField("AcctCretDt").contains("#S")) {
		Cnts.setAcctCretDt(rs.getField("AcctCretDt"));
	}else if(rs.getField("AcctCretDt").equalsIgnoreCase("M")) {
		Cnts.setAcctCretDt("");
	}else if(rs.getField("AcctCretDt").isEmpty()) {
		
	}else if(rs.getField("AcctCretDt").contains("#I")){
		Cnts.setAcctCretDt(rs.getField("AcctCretDt").substring(2,rs.getField("AcctCretDt").length()));
	}else if(rs.getField("AcctCretDt").contains("#S")){
		Cnts.setAcctCretDt(rs.getField("AcctCretDt").substring(2,rs.getField("AcctCretDt").length()));
		Cnts.setAcctCretDt(rs.getField("AcctCretDt").substring(2,rs.getField("AcctCretDt").length()));
	}
	
	if (!rs.getField("chargesAmount").isEmpty() && !rs.getField("chargesAmount").equalsIgnoreCase("M") && !rs.getField("chargesAmount").equalsIgnoreCase("I") && !rs.getField("chargesAmount").contains("#I") && !rs.getField("chargesAmount").contains("#S")) {
		Cnts.setChargesAmount(rs.getField("chargesAmount"));
	}else if(rs.getField("chargesAmount").equalsIgnoreCase("M")) {
		Cnts.setChargesAmount("");
	}else if(rs.getField("chargesAmount").isEmpty()) {
		
	}else if(rs.getField("chargesAmount").contains("#I")){
		Cnts.setChargesAmount(rs.getField("chargesAmount").substring(2,rs.getField("chargesAmount").length()));
	}else if(rs.getField("chargesAmount").contains("#S")){
		Cnts.setChargesAmount(rs.getField("chargesAmount").substring(2,rs.getField("chargesAmount").length()));
		Cnts.setChargesAmount(rs.getField("chargesAmount").substring(2,rs.getField("chargesAmount").length()));
	}
	
//	originatorFIRTPRefNum
	if (!rs.getField("originatorFIRTPRefNum").isEmpty() && !rs.getField("originatorFIRTPRefNum").equalsIgnoreCase("M") && !rs.getField("originatorFIRTPRefNum").equalsIgnoreCase("I") && !rs.getField("originatorFIRTPRefNum").contains("#I") && !rs.getField("originatorFIRTPRefNum").contains("#S")) {
		Cnts.setOriginatorFIRTPRefNum(rs.getField("originatorFIRTPRefNum"));
	}else if(rs.getField("originatorFIRTPRefNum").equalsIgnoreCase("M")) {
		Cnts.setOriginatorFIRTPRefNum("");
	}else if(rs.getField("originatorFIRTPRefNum").isEmpty()) {
		
	}else if(rs.getField("originatorFIRTPRefNum").contains("#I")){
		Cnts.setOriginatorFIRTPRefNum(rs.getField("originatorFIRTPRefNum").substring(2,rs.getField("originatorFIRTPRefNum").length()));
	}else if(rs.getField("originatorFIRTPRefNum").contains("#S")){
		Cnts.setOriginatorFIRTPRefNum(rs.getField("originatorFIRTPRefNum").substring(2,rs.getField("originatorFIRTPRefNum").length()));
		Cnts.setOriginatorFIRTPRefNum(rs.getField("originatorFIRTPRefNum").substring(2,rs.getField("originatorFIRTPRefNum").length()));
	}
	
	
//	interacRTPRefNum
	if (!rs.getField("interacRTPRefNum").isEmpty() && !rs.getField("interacRTPRefNum").equalsIgnoreCase("M") && !rs.getField("interacRTPRefNum").equalsIgnoreCase("I") && !rs.getField("interacRTPRefNum").contains("#I") && !rs.getField("interacRTPRefNum").contains("#S")) {
		Cnts.setInteracRTPRefNum(rs.getField("interacRTPRefNum"));
	}else if(rs.getField("interacRTPRefNum").equalsIgnoreCase("M")) {
		Cnts.setInteracRTPRefNum("");
	}else if(rs.getField("interacRTPRefNum").isEmpty()) {
		
	}else if(rs.getField("interacRTPRefNum").contains("#I")){
		Cnts.setInteracRTPRefNum(rs.getField("interacRTPRefNum").substring(2,rs.getField("interacRTPRefNum").length()));
	}else if(rs.getField("interacRTPRefNum").contains("#S")){
		Cnts.setInteracRTPRefNum(rs.getField("interacRTPRefNum").substring(2,rs.getField("interacRTPRefNum").length()));
		Cnts.setInteracRTPRefNum(rs.getField("interacRTPRefNum").substring(2,rs.getField("interacRTPRefNum").length()));
	}
	
}

public static void DbtrAgtSetID(Recordset rs, DbtrAgtOthr DbtrAgtOthr) throws FilloException {
	if (!rs.getField("Id_3value").isEmpty() && !rs.getField("Id_3value").equalsIgnoreCase("M") && !rs.getField("Id_3value").equalsIgnoreCase("I") && !rs.getField("Id_3value").contains("#I") && !rs.getField("Id_3value").contains("#S")) {
		DbtrAgtOthr.setId(rs.getField("Id_3value"));
	}else if(rs.getField("Id_3value").equalsIgnoreCase("M")) {
		DbtrAgtOthr.setId("");
	}else if(rs.getField("Id_3value").isEmpty()) {
		
	}else if(rs.getField("Id_3value").contains("#I")){
		DbtrAgtOthr.setIdX(rs.getField("Id_3value").substring(2,rs.getField("Id_3value").length()));
	}else if(rs.getField("Id_3value").contains("#S")){
		DbtrAgtOthr.setIdX(rs.getField("Id_3value").substring(2,rs.getField("Id_3value").length()));
		DbtrAgtOthr.setId(rs.getField("Id_3value").substring(2,rs.getField("Id_3value").length()));
	}
}

public static void getDbtrCtry(Recordset rs, DbtrAgtPstlAdr DbtrAgtPstlAdr) throws FilloException {
	
	if (!rs.getField("Ctry_value1").isEmpty() && !rs.getField("Ctry_value1").equalsIgnoreCase("M") && !rs.getField("Ctry_value1").equalsIgnoreCase("I") && !rs.getField("Ctry_value1").contains("#I") && !rs.getField("Ctry_value1").contains("#S")) {
		DbtrAgtPstlAdr.setCtry(rs.getField("Ctry_value1"));
	}else if(rs.getField("Ctry_value1").equalsIgnoreCase("M")) {
		DbtrAgtPstlAdr.setCtry("");
	}else if(rs.getField("Ctry_value1").isEmpty()) {
		
	}else if(rs.getField("Ctry_value1").contains("#I")){
		DbtrAgtPstlAdr.setCtryX(rs.getField("Ctry_value1").substring(2,rs.getField("Ctry_value1").length()));
	}else if(rs.getField("Ctry_value1").contains("#S")){
		DbtrAgtPstlAdr.setCtryX(rs.getField("Ctry_value1").substring(2,rs.getField("Ctry_value1").length()));
		DbtrAgtPstlAdr.setCtry(rs.getField("Ctry_value1").substring(2,rs.getField("Ctry_value1").length()));
	}
}

public static void getIBAN(Recordset rs, CdtrAcctId CdtrAcctId) throws FilloException {
	
	if (!rs.getField("IBAN").isEmpty() && !rs.getField("IBAN").equalsIgnoreCase("M") && !rs.getField("IBAN").equalsIgnoreCase("I") && !rs.getField("IBAN").contains("#I") && !rs.getField("IBAN").contains("#S")) {
		CdtrAcctId.setIBAN(rs.getField("IBAN"));
	}else if(rs.getField("IBAN").equalsIgnoreCase("M")) {
		CdtrAcctId.setIBAN("");
	}else if(rs.getField("IBAN").isEmpty()) {
		
	}else if(rs.getField("IBAN").contains("#I")){
		CdtrAcctId.setIBANX(rs.getField("IBAN").substring(2,rs.getField("IBAN").length()));
	}else if(rs.getField("IBAN").contains("#S")){
		CdtrAcctId.setIBANX(rs.getField("IBAN").substring(2,rs.getField("IBAN").length()));
		CdtrAcctId.setIBAN(rs.getField("IBAN").substring(2,rs.getField("IBAN").length()));
	}
}

public static void getAnyBIC(Recordset rs, DbtrOrgId DbtrOrgId) throws FilloException {
		if (!rs.getField("AnyBIC").isEmpty() && !rs.getField("AnyBIC").equalsIgnoreCase("M") && !rs.getField("AnyBIC").equalsIgnoreCase("I") && !rs.getField("AnyBIC").contains("#I") && !rs.getField("AnyBIC").contains("#S")) {
			DbtrOrgId.setAnyBIC(rs.getField("AnyBIC"));
		}else if(rs.getField("AnyBIC").equalsIgnoreCase("M")) {
			DbtrOrgId.setAnyBIC("");
		}else if(rs.getField("AnyBIC").isEmpty()) {
			
		}else if(rs.getField("AnyBIC").contains("#I")){
			DbtrOrgId.setAnyBICX(rs.getField("AnyBIC").substring(2,rs.getField("AnyBIC").length()));
		}else if(rs.getField("AnyBIC").contains("#S")){
			DbtrOrgId.setAnyBICX(rs.getField("AnyBIC").substring(2,rs.getField("AnyBIC").length()));
			DbtrOrgId.setAnyBIC(rs.getField("AnyBIC").substring(2,rs.getField("AnyBIC").length()));
		}
	}

public static void SetChargesAccountId(Recordset rs, ChrgsAcctOthr Othr1) throws FilloException {
		if (!rs.getField("Id_1value").isEmpty() && !rs.getField("Id_1value").equalsIgnoreCase("M") && !rs.getField("Id_1value").equalsIgnoreCase("I") && !rs.getField("Id_1value").contains("#I") && !rs.getField("Id_1value").contains("#S")) {
			Othr1.setId(rs.getField("Id_1value"));
		}else if(rs.getField("Id_1value").equalsIgnoreCase("M")) {
			Othr1.setId("");
		}else if(rs.getField("Id_1value").isEmpty()) {
			
		}else if(rs.getField("Id_1value").contains("#I")){
			Othr1.setIdX(rs.getField("Id_1value").substring(2,rs.getField("Id_1value").length()));
		}else if(rs.getField("Id_1value").contains("#S")){
			Othr1.setIdX(rs.getField("Id_1value").substring(2,rs.getField("Id_1value").length()));
			Othr1.setId(rs.getField("Id_1value").substring(2,rs.getField("Id_1value").length()));
		}
	}

public static void getBICFI(Recordset rs, FinInstnId FinInstnId) throws FilloException {
	
	if (!rs.getField("_BICFI").isEmpty() && !rs.getField("_BICFI").equalsIgnoreCase("M") && !rs.getField("_BICFI").equalsIgnoreCase("I") && !rs.getField("_BICFI").contains("#I") && !rs.getField("_BICFI").contains("#S")) {
		FinInstnId.setBICFI(rs.getField("_BICFI"));
	}else if(rs.getField("_BICFI").equalsIgnoreCase("M")) {
		FinInstnId.setBICFI("");
	}else if(rs.getField("_BICFI").isEmpty()) {
		
	}else if(rs.getField("_BICFI").contains("#I")){
		FinInstnId.setBICFIX(rs.getField("_BICFI").substring(2,rs.getField("_BICFI").length()));
	}else if(rs.getField("_BICFI").contains("#S")){
		FinInstnId.setBICFIX(rs.getField("_BICFI").substring(2,rs.getField("_BICFI").length()));
		FinInstnId.setBICFI(rs.getField("_BICFI").substring(2,rs.getField("_BICFI").length()));
	}
}

public static void getPurp(Recordset rs, Purp Purp) throws FilloException {
	if(!rs.getField("Prtry_value").isEmpty() && !rs.getField("Prtry_value").equalsIgnoreCase("M") && !rs.getField("Prtry_value").equalsIgnoreCase("I") && !rs.getField("Prtry_value").contains("#I") && !rs.getField("Prtry_value").contains("#S")) {
		Purp.setPrtry(rs.getField("Prtry_value"));
	}else if(rs.getField("Prtry_value").equalsIgnoreCase("M")) {
		Purp.setPrtry("");
	}else if(rs.getField("Prtry_value").isEmpty()) {
		
	}else if(rs.getField("Prtry_value").contains("#I")){
		Purp.setPrtryX(rs.getField("Prtry_value").substring(2,rs.getField("Prtry_value").length()));
	}else if(rs.getField("Prtry_value").contains("#S")){
		Purp.setPrtryX(rs.getField("Prtry_value").substring(2,rs.getField("Prtry_value").length()));
		Purp.setPrtry(rs.getField("Prtry_value").substring(2,rs.getField("Prtry_value").length()));
	}
}

public static void getInstrForDbtrAgt(Recordset rs, CdtTrfTxInf CdtTrfTxInf) throws FilloException {
	if (!rs.getField("InstrForDbtrAgt").isEmpty() && !rs.getField("InstrForDbtrAgt").equalsIgnoreCase("M") && !rs.getField("InstrForDbtrAgt").equalsIgnoreCase("I") && !rs.getField("InstrForDbtrAgt").contains("#I") && !rs.getField("InstrForDbtrAgt").contains("#S")) {
		CdtTrfTxInf.setInstrForDbtrAgt(rs.getField("InstrForDbtrAgt"));
	}else if(rs.getField("InstrForDbtrAgt").equalsIgnoreCase("M")) {
		CdtTrfTxInf.setInstrForDbtrAgt("");
	}else if(rs.getField("InstrForDbtrAgt").isEmpty()) {
		
	}else if(rs.getField("InstrForDbtrAgt").contains("#I")){
		CdtTrfTxInf.setInstrForDbtrAgtX(rs.getField("InstrForDbtrAgt").substring(2,rs.getField("InstrForDbtrAgt").length()));
	}else if(rs.getField("InstrForDbtrAgt").contains("#S")){
		CdtTrfTxInf.setInstrForDbtrAgtX(rs.getField("InstrForDbtrAgt").substring(2,rs.getField("InstrForDbtrAgt").length()));
		CdtTrfTxInf.setInstrForDbtrAgt(rs.getField("InstrForDbtrAgt").substring(2,rs.getField("InstrForDbtrAgt").length()));
	}
 }

}
