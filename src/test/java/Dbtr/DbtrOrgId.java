package Dbtr;

public class DbtrOrgId {
	private String AnyBIC;
	private String AnyBICX;
	private DbtrOthr Othr;
	
	
	public String getAnyBICX() {
		return AnyBICX;
	}

	public void setAnyBICX(String anyBICX) {
		AnyBICX = anyBICX;
	}

	public String getAnyBIC() {
		return AnyBIC;
	}

	public void setAnyBIC(String anyBIC) {
		AnyBIC = anyBIC;
	}

	public DbtrOthr getOthr() {
		return Othr;
	}

	public void setOthr(DbtrOthr othr) {
		Othr = othr;
	}
	
}
