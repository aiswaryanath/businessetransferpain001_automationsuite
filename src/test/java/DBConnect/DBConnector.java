package DBConnect;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import Busness_Validation.Batch_ProcessValidation;
import Busness_Validation.ExecuteQuery;
import Busness_Validation.GtbExcepGridDetailvalidation;
import Busness_Validation.TxnProcOpnValidation;
import Busness_Validation.psh_response_messagesValidation;
import ParseRecordExcel.Update_ExcelResult;
import ReadDBConfiguration.ReadDB;

public class DBConnector {
	public static void DB_UpdateConnector(String SQL_Query){
		Connection connection=null;
		Statement stmt=null;
		try {	
					Class.forName(ReadDB.getENV1_OracleDriver());
					connection=DriverManager.getConnection(ReadDB.getENV1_ConnName(),ReadDB.getENV1_Uname(),ReadDB.getENV1_Pass());
					stmt = connection.createStatement();
					System.out.println("Udpate query executed Successfully!!"+SQL_Query);
					stmt.executeUpdate(SQL_Query);
			}catch (Exception e) {
				e.printStackTrace();
			}

	}
	
}
