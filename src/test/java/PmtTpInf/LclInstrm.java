package PmtTpInf;

public class LclInstrm {
	private String Cd;
	private String CdX;
	private String Prtry;
	public String getPrtry() {
		return Prtry;
	}

	public void setPrtry(String prtry) {
		Prtry = prtry;
	}
	public String getCdX() {
		return CdX;
	}

	public void setCdX(String cdX) {
		CdX = cdX;
	}

	public String getCd() {
		return Cd;
	}

	public void setCd(String cd) {
		Cd = cd;
	}
}
