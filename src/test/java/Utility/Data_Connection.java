package Utility;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import com.codoid.products.fillo.Connection;
import com.codoid.products.fillo.Fillo;
import com.codoid.products.fillo.Recordset;

import Constants_pack.Constants;

public class Data_Connection {
	
	public static Connection getConnection(Connection connection) throws FileNotFoundException, IOException {
		try{
			Fillo fill=new Fillo();
			connection = fill.getConnection(Constants._ExcelPath);
		}catch (Exception e) {
			e.getStackTrace();
		}	
		return connection;
	}

	public static final Recordset executeQuery(final Connection connection,String _str) {
	    try {
	        return connection.executeQuery(_str);
	    } catch (Exception e) {
	        throw new RuntimeException(e);
	    }
	}
	
	
	public static void SuccessfullClickJS(WebDriver driver,String Locator){
		try{
		int count =0;
		 while(count<10){
			 if (driver.findElement(By.xpath(Locator)).isDisplayed()) {
				 System.out.println("Element found!!");
				 	((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath(Locator)));
				 break;
			}else{
				System.out.println(count);
				count ++;
			}
		 }
	}catch (Exception e) {
		System.out.println("Exception checking again!!"+e.toString());
//		HandleWebElements.SuccessfullClickFE(driver, Locator);
	}
  }
	
	
}




