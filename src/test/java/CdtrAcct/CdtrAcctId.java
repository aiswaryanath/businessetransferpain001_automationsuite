package CdtrAcct;

public class CdtrAcctId {
	private CdtrAcctOthr Othr;
	private String IBAN;
	private String IBANX;
	
	public String getIBANX() {
		return IBANX;
	}

	public void setIBANX(String iBANX) {
		IBANX = iBANX;
	}

	public String getIBAN() {
		return IBAN;
	}

	public void setIBAN(String iBAN) {
		IBAN = iBAN;
	}

	public CdtrAcctOthr getOthr() {
		return Othr;
	}

	public void setOthr(CdtrAcctOthr othr) {
		Othr = othr;
	}
}
