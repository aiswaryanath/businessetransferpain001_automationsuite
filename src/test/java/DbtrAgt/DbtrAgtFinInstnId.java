package DbtrAgt;

public class DbtrAgtFinInstnId {
	private String BICFI;
	private String BICFIX;
	private String Nm;
	private String NmX;
	
	public String getNmX() {
		return NmX;
	}
	public void setNmX(String nmX) {
		NmX = nmX;
	}
	public String getBICFIX() {
		return BICFIX;
	}
	public void setBICFIX(String bICFIX) {
		BICFIX = bICFIX;
	}
	private DbtrAgtPstlAdr PstlAdr;
	private DbtrAgtOthr Othr;
	
	public String getBICFI() {
		return BICFI;
	}
	public void setBICFI(String bICFI) {
		BICFI = bICFI;
	}
	public String getNm() {
		return Nm;
	}
	public void setNm(String nm) {
		Nm = nm;
	}
	public DbtrAgtPstlAdr getPstlAdr() {
		return PstlAdr;
	}
	public void setPstlAdr(DbtrAgtPstlAdr pstlAdr) {
		PstlAdr = pstlAdr;
	}
	public DbtrAgtOthr getOthr() {
		return Othr;
	}
	public void setOthr(DbtrAgtOthr othr) {
		Othr = othr;
	}
	
}
