package DbtrAgt;

public class DbtrAgt {
	private DbtrAgtFinInstnId FinInstnId;
	private DbtrAgtBrnchId BrnchId;
	public DbtrAgtFinInstnId getFinInstnId() {
		return FinInstnId;
	}
	public void setFinInstnId(DbtrAgtFinInstnId finInstnId) {
		FinInstnId = finInstnId;
	}
	public DbtrAgtBrnchId getBrnchId() {
		return BrnchId;
	}
	public void setBrnchId(DbtrAgtBrnchId brnchId) {
		BrnchId = brnchId;
	}
}
