package APIGsonKeyValueConditions;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Recordset;

import PmtId.PmtId;
import Utility.SaltString;

public class PmtIdGson {
	
	public static void getPmtID(Recordset rs, PmtId PmtId) throws FilloException {
		
		if (!rs.getField("InstrId_value").isEmpty() && !rs.getField("InstrId_value").equalsIgnoreCase("M") && !rs.getField("InstrId_value").equalsIgnoreCase("I") && !rs.getField("InstrId_value").contains("#I") && !rs.getField("InstrId_value").contains("#S")) {
			PmtId.setInstrId(rs.getField("InstrId_value"));
		}else if(rs.getField("InstrId_value").equalsIgnoreCase("M")) {
			PmtId.setInstrId("");
		}else if(rs.getField("InstrId_value").isEmpty()) {
			
		}else if(rs.getField("InstrId_value").contains("#I")){
			PmtId.setInstrIdX(rs.getField("InstrId_value").substring(2,rs.getField("InstrId_value").length()));
		}else if(rs.getField("InstrId_value").contains("#S")){
			PmtId.setInstrIdX(rs.getField("InstrId_value").substring(2,rs.getField("InstrId_value").length()));
			PmtId.setInstrId(rs.getField("InstrId_value").substring(2,rs.getField("InstrId_value").length()));
		}
		
		
		if (!rs.getField("EndToEndId_value").isEmpty() && !rs.getField("EndToEndId_value").equalsIgnoreCase("M") && !rs.getField("EndToEndId_value").equalsIgnoreCase("I") && !rs.getField("EndToEndId_value").contains("#I") && !rs.getField("EndToEndId_value").contains("#S")) {
//			PmtId.setEndToEndId(rs.getField("EndToEndId_value"));
			if (rs.getField("EndToEndId_value").contains("#E2E")) {
				String EndtoEndId  = rs.getField("EndToEndId_value").substring(2,rs.getField("EndToEndId_value").length());
				PmtId.setEndToEndId(EndtoEndId);
			}else{
				PmtId.setEndToEndId(SaltString.getRandkeys(10));
			}
		}else if(rs.getField("EndToEndId_value").equalsIgnoreCase("M")) {
			PmtId.setEndToEndId("");
		}else if(rs.getField("EndToEndId_value").isEmpty()) {
			
		}else if(rs.getField("EndToEndId_value").contains("#I")){
			PmtId.setEndToEndIdX(rs.getField("EndToEndId_value").substring(2,rs.getField("EndToEndId_value").length()));
		}else if(rs.getField("EndToEndId_value").contains("#S")){
			PmtId.setEndToEndIdX(rs.getField("EndToEndId_value").substring(2,rs.getField("EndToEndId_value").length()));
			PmtId.setEndToEndId(rs.getField("EndToEndId_value").substring(2,rs.getField("EndToEndId_value").length()));
		}
	}
}
