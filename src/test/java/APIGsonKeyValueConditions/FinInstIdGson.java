package APIGsonKeyValueConditions;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Recordset;

import CdtrAgt.FinInstnId;

public class FinInstIdGson {
	public static void getFinInstnIdSetNm(Recordset rs, FinInstnId FinInstnId) throws FilloException {
		
		if (!rs.getField("Nm_value").isEmpty() && !rs.getField("Nm_value").equalsIgnoreCase("M") && !rs.getField("Nm_value").equalsIgnoreCase("I") && !rs.getField("Nm_value").contains("#I") && !rs.getField("Nm_value").contains("#S")) {
			FinInstnId.setNm(rs.getField("Nm_value"));
		}else if(rs.getField("Nm_value").equalsIgnoreCase("M")) {
			FinInstnId.setNm("");
		}else if(rs.getField("Nm_value").isEmpty()) {
			
		}else if(rs.getField("Nm_value").contains("#I")){
			FinInstnId.setNmX(rs.getField("Nm_value").substring(2,rs.getField("Nm_value").length()));
		}else if(rs.getField("Nm_value").contains("#S")){
			FinInstnId.setNmX(rs.getField("Nm_value").substring(2,rs.getField("Nm_value").length()));
			FinInstnId.setNm(rs.getField("Nm_value").substring(2,rs.getField("Nm_value").length()));
		}

		
	}
}
