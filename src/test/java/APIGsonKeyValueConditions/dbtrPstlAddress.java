package APIGsonKeyValueConditions;

import java.util.ArrayList;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Recordset;

public class dbtrPstlAddress {
	public static void getDbtrPstlAddress(Recordset rs3, ArrayList Address) throws FilloException {
		if (!rs3.getField("AdrLine1").isEmpty()) {
			Address.add(rs3.getField("AdrLine1"));
		}
		if (!rs3.getField("AdrLine2").isEmpty()) {
			Address.add(rs3.getField("AdrLine2"));
		}
		if (!rs3.getField("AdrLine3").isEmpty()) {
			Address.add(rs3.getField("AdrLine3"));
		}
	}

}
