package APIGsonKeyValueConditions;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Recordset;

import CdtrAcct.CdtrAcctOthr;

public class CdtrAcctGson {
	public static void getCdtrAccIdNm(Recordset rs, CdtrAcctOthr CdtrAcctOthr) throws FilloException {
		if (!rs.getField("Id_value1").isEmpty() && !rs.getField("Id_value1").equalsIgnoreCase("M") && !rs.getField("Id_value1").equalsIgnoreCase("I") && !rs.getField("Id_value1").contains("#I") && !rs.getField("Id_value1").contains("#S")) {
			CdtrAcctOthr.setId(rs.getField("Id_value1"));
		}else if(rs.getField("Id_value1").equalsIgnoreCase("M")) {
			CdtrAcctOthr.setId("");
		}else if(rs.getField("Id_value1").isEmpty()) {
			
		}else if(rs.getField("Id_value1").contains("#I")){
			CdtrAcctOthr.setIdX(rs.getField("Id_value1").substring(2,rs.getField("Id_value1").length()));
		}else if(rs.getField("Id_value1").contains("#S")){
			CdtrAcctOthr.setIdX(rs.getField("Id_value1").substring(2,rs.getField("Id_value1").length()));
			CdtrAcctOthr.setId(rs.getField("Id_value1").substring(2,rs.getField("Id_value1").length()));
		}
	}
	
	
}
