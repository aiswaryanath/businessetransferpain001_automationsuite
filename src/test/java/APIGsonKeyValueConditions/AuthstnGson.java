package APIGsonKeyValueConditions;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Recordset;

import JSONStructure_PAIN001.Authstn;

public class AuthstnGson {
	
	public static void GetAuthstn(Recordset rs, Authstn Authstn) throws FilloException {
		if (!rs.getField("Prtry_value").isEmpty() && !rs.getField("Prtry_value").equalsIgnoreCase("M") && !rs.getField("Prtry_value").equalsIgnoreCase("I") && !rs.getField("Prtry_value").contains("#I") && !rs.getField("Prtry_value").contains("#S")) {
			Authstn.setPrtry(rs.getField("Prtry_value"));
		}else if(rs.getField("Prtry_value").equalsIgnoreCase("M")) {
			Authstn.setPrtry("");
		}else if(rs.getField("Prtry_value").isEmpty()) {
			
		}else if(rs.getField("Prtry_value").contains("#I")){
			Authstn.setPrtryX(rs.getField("Prtry_value").substring(2,rs.getField("Prtry_value").length()));
		}else if(rs.getField("Prtry_value").contains("#S")){
			Authstn.setPrtryX(rs.getField("Prtry_value").substring(2,rs.getField("Prtry_value").length()));
			Authstn.setPrtry(rs.getField("Prtry_value").substring(2,rs.getField("Prtry_value").length()));
		}
	}

	
}
