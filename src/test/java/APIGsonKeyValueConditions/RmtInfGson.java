package APIGsonKeyValueConditions;

import java.util.ArrayList;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Recordset;

import RmtInf.RmtInf;

public class RmtInfGson {
	public static void getRmtInf(Recordset rs, RmtInf RmtInf) throws FilloException {
		ArrayList<String> remitinfo=new ArrayList<String>();
		//RmtInf.getUstrd(remitinfo);

		
		if (!rs.getField("RmtInf_value1").isEmpty()) {
			remitinfo.add(rs.getField("RmtInf_value1"));
			RmtInf.setUstrd(remitinfo);

		}
		if (!rs.getField("RmtInf_value2").isEmpty()) {
			remitinfo.add(rs.getField("RmtInf_value2"));
			RmtInf.setUstrd(remitinfo);

		}
		if (!rs.getField("RmtInf_value3").isEmpty()) {
			remitinfo.add(rs.getField("RmtInf_value3"));
			RmtInf.setUstrd(remitinfo);

		}
	}
}
