package APIGsonKeyValueConditions;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Recordset;

import Cdtr.Cdtr;
import Cdtr.CdtrPstlAdr;

public class CdtrGson {
	public static void getCdtrPstlAdr(Recordset rs, CdtrPstlAdr CdtrPstlAdr) throws FilloException {
		if (!rs.getField("Ctry_value1").isEmpty() && !rs.getField("Ctry_value1").equalsIgnoreCase("M") && !rs.getField("Ctry_value1").equalsIgnoreCase("I") && !rs.getField("Ctry_value1").contains("#I") && !rs.getField("Ctry_value1").contains("#S")) {
			CdtrPstlAdr.setCtry(rs.getField("Ctry_value1"));
		}else if(rs.getField("Ctry_value1").equalsIgnoreCase("M")) {
			CdtrPstlAdr.setCtry("");
		}else if(rs.getField("Ctry_value1").isEmpty()) {
			
		}else if(rs.getField("Ctry_value1").contains("#I")){
			CdtrPstlAdr.setCtryX(rs.getField("Ctry_value1").substring(2,rs.getField("Ctry_value1").length()));
		}else if(rs.getField("Ctry_value1").contains("#S")){
			CdtrPstlAdr.setCtryX(rs.getField("Ctry_value1").substring(2,rs.getField("Ctry_value1").length()));
			CdtrPstlAdr.setCtry(rs.getField("Ctry_value1").substring(2,rs.getField("Ctry_value1").length()));
		}
		
		
		if (!rs.getField("StrtNm_value1").isEmpty() && !rs.getField("StrtNm_value1").equalsIgnoreCase("M") && !rs.getField("StrtNm_value1").equalsIgnoreCase("I") && !rs.getField("StrtNm_value1").contains("#I") && !rs.getField("StrtNm_value1").contains("#S")) {
			CdtrPstlAdr.setStrtNm(rs.getField("StrtNm_value1"));
		}else if(rs.getField("StrtNm_value1").equalsIgnoreCase("M")) {
			CdtrPstlAdr.setStrtNm("");
		}else if(rs.getField("StrtNm_value1").isEmpty()) {
			
		}else if(rs.getField("StrtNm_value1").contains("#I")){
			CdtrPstlAdr.setStrtNmX(rs.getField("StrtNm_value1").substring(2,rs.getField("StrtNm_value1").length()));
		}else if(rs.getField("StrtNm_value1").contains("#S")){
			CdtrPstlAdr.setStrtNmX(rs.getField("StrtNm_value1").substring(2,rs.getField("StrtNm_value1").length()));
			CdtrPstlAdr.setStrtNm(rs.getField("StrtNm_value1").substring(2,rs.getField("StrtNm_value1").length()));
		}
		
		
	}
	
	
	public static void getCdtrNm(Recordset rs, Cdtr Cdtr) throws FilloException {
		
		if (!rs.getField("Nm_value1").isEmpty() && !rs.getField("Nm_value1").equalsIgnoreCase("M") && !rs.getField("Nm_value1").equalsIgnoreCase("I") && !rs.getField("Nm_value1").contains("#I") && !rs.getField("Nm_value1").contains("#S")) {
			Cdtr.setNm(rs.getField("Nm_value1"));
		}else if(rs.getField("Nm_value1").equalsIgnoreCase("M")) {
			Cdtr.setNm("");
		}else if(rs.getField("Nm_value1").isEmpty()) {
			
		}else if(rs.getField("Nm_value1").contains("#I")){
			Cdtr.setNmX(rs.getField("Nm_value1").substring(2,rs.getField("Nm_value1").length()));
		}else if(rs.getField("Nm_value1").contains("#S")){
			Cdtr.setNmX(rs.getField("Nm_value1").substring(2,rs.getField("Nm_value1").length()));
			Cdtr.setNm(rs.getField("Nm_value1").substring(2,rs.getField("Nm_value1").length()));
		}
		
	}
	
	
		
}
