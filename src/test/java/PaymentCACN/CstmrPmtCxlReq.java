package PaymentCACN;

import Assgnmt.Assgnmt;
import Assgnmt.CtrlData;
import SplmtryData.SplmtryData;
import Undrlyg.Undrlyg;

public class CstmrPmtCxlReq {
	private Assgnmt Assgnmt;
	private CtrlData CtrlData;
	private Undrlyg Undrlyg;
	private SplmtryData SplmtryData;


	public SplmtryData getSp() {
		return SplmtryData;
	}
	public void setSp(SplmtryData sp) {
		this.SplmtryData = sp;
	}
	public Undrlyg getUndrlyg() {
		return Undrlyg;
	}
	public void setUndrlyg(Undrlyg undrlyg) {
		Undrlyg = undrlyg;
	}
	public CtrlData getCtrlData() {
		return CtrlData;
	}
	public void setCtrlData(CtrlData ctrlData) {
		CtrlData = ctrlData;
	}
	public Assgnmt getAssgnmt() {
		return Assgnmt;
	}
	public void setAssgnmt(Assgnmt assgnmt) {
		Assgnmt = assgnmt;
	}
	
}
