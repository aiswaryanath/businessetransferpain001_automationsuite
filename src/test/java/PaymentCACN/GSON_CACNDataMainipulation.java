package PaymentCACN;

import java.io.FileWriter;
import java.io.IOException;

import org.json.JSONException;

import com.codoid.products.exception.FilloException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import Assgnmt.Assgnmt;
import Assgnmt.Assgnr;
import Assgnmt.Pty;
import InitgPty.Id;
import InitgPty.OrgId;
import InitgPty.Othr;
import Undrlyg.Cretr;
import Undrlyg.CxlRsnInf;
import Undrlyg.OrgnlInstdAmt;
import Undrlyg.OrgnlPmtInfAndCxl;
import Undrlyg.TxInf;
import Undrlyg.Undrlyg;
import Utility.SaltString;

public class GSON_CACNDataMainipulation {
	
	public static void Cancellation(String TESTCASEID) throws IOException, JSONException, FilloException {
		DocumentCACN Document = new DocumentCACN();  
		CstmrPmtCxlReq CstmrPmtCxlReq=new CstmrPmtCxlReq();

//Assgnmt
		Assgnmt Assgnmt=new Assgnmt();
		Pty _Pty=new Pty();
		Id Id=new Id();
		OrgId OrgId=new OrgId();
		Othr Othr=new Othr();
		Othr.setId("90000017");
		OrgId.setOthr(Othr);
		Id.setOrgId(OrgId);
		
		_Pty.setId(Id);
		Assgnr Assgnr=new Assgnr();
		Assgnr.setPty(_Pty);
		
		Assgnmt.setAssgnr(Assgnr);
		
		Assgnmt.setId("CACN"+SaltString.getSaltString(5));
		
		CstmrPmtCxlReq.setAssgnmt(Assgnmt);
		
		Undrlyg Undrlyg=new Undrlyg();
		OrgnlPmtInfAndCxl OrgnlPmtInfAndCxl=new OrgnlPmtInfAndCxl();
		OrgnlPmtInfAndCxl.setOrgnlPmtInfId("SAGARxBTCHx25062019x002");
		TxInf TxInf=new TxInf();
		TxInf.setCxlId("Croatia777");
		System.out.println("CANCELAPI::"+CancelAPI.GetEndToEndID(TESTCASEID).get(0));
		TxInf.setOrgnlEndToEndId(CancelAPI.GetEndToEndID(TESTCASEID).get(0));
		
		Pty Pty=new Pty(); 
		Cretr Cretr=new Cretr();
		
		Pty.setNm("WIRECATT");
		
		Id Id1=new Id();
		OrgId OrgId1=new OrgId();
		Othr Othr1=new Othr();
		Othr1.setId("90000004");
		OrgId1.setOthr(Othr1);
		Id1.setOrgId(OrgId1);
		
		Pty.setId(Id1);
		Cretr.setPty(Pty);
		
		
		OrgnlInstdAmt OrgnlInstdAmt=new OrgnlInstdAmt();
		OrgnlInstdAmt.setCcy("CAD");
		OrgnlInstdAmt.setText("100.00");
		
		CxlRsnInf CxlRsnInf=new CxlRsnInf();
		CxlRsnInf.setAddtlInf("DO IT.");
		TxInf.setCxlRsnInf(CxlRsnInf);
		
		//TxInf add here
		OrgnlPmtInfAndCxl.setTxInf(TxInf);	
		
		Undrlyg.setOrgnlPmtInfAndCxl(OrgnlPmtInfAndCxl);
		CstmrPmtCxlReq.setUndrlyg(Undrlyg);
        	
		Document.setCstmrPmtCxlReq(CstmrPmtCxlReq);
		
		RootCACN RootCACN=new RootCACN();
		RootCACN.setDocumentCACN(Document);
		
		Gson Gson =new GsonBuilder().setPrettyPrinting().create();
		JsonElement str=Gson.toJsonTree(RootCACN);
		String prettyJsonString = Gson.toJson(str);

	     FileWriter fw=new FileWriter("D:\\BusinessETransfer_Automation\\_Output\\"+TESTCASEID+"_CACN_Pain001_REQ.json");    
           fw.write(prettyJsonString);    
         fw.close();
	}
	public static void main(String[] args) throws Exception{
		Cancellation("Cancellation_BET");
	}
	
}
