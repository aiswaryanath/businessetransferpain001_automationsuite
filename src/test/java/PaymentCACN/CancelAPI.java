package PaymentCACN;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class CancelAPI {
	
	public static String readFile(String filename) {
	    String result = "";
	    try {
	        BufferedReader br = new BufferedReader(new FileReader(filename));
	        StringBuilder sb = new StringBuilder();
	        String line = br.readLine();
	        while (line != null) {
	            sb.append(line);
	            line = br.readLine();
	        }
	        result = sb.toString();
	    } catch(Exception e) {
	        e.printStackTrace();
	    }
	    return result;
	}
	
	public static ArrayList<String> GetEndToEndID(String TESTCASEID) throws JSONException{
		ArrayList<String> _add_pmtEndToendId = new ArrayList<String>();  
		try{
		String toString_BETJSON = readFile("D:\\BusinessETransfer_Automation\\_Output\\"+TESTCASEID+"_Pain001_REQ.json");
		JSONObject Document = new JSONObject(toString_BETJSON);
		String CstmrCdtTrfInitn  = Document.get("Document").toString();
		JSONObject _GrpHdr = new JSONObject(CstmrCdtTrfInitn);
		String GrpHdr = _GrpHdr.get("CstmrCdtTrfInitn").toString();
		JSONObject getVal = new JSONObject(GrpHdr);
		JSONArray arr= getVal.getJSONArray("PmtInf");
		for (int i = 0; i < arr.length(); i++) {
			System.out.println("-->"+arr.getJSONObject(i).getJSONArray("CdtTrfTxInf"));
			for (int j = 0; j < arr.getJSONObject(i).getJSONArray("CdtTrfTxInf").length(); j++) {
				JSONArray StsRsnInf_arr = arr.getJSONObject(i).getJSONArray("CdtTrfTxInf");
				System.out.println("Length-->"+StsRsnInf_arr.length());
				for (int k = 0; k < StsRsnInf_arr.length(); k++) {
					JSONObject PmtEndtoEndID = new JSONObject(StsRsnInf_arr.get(0).toString());
					System.out.println("PmtId:"+PmtEndtoEndID.get("PmtId"));
					String EndToEndId = PmtEndtoEndID.get("PmtId").toString();
					JSONObject getEndToEndId = new JSONObject(EndToEndId);
					System.out.println(getEndToEndId.get("EndToEndId"));
					_add_pmtEndToendId.add(getEndToEndId.get("EndToEndId").toString());
				}
			}
		}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return _add_pmtEndToendId;
	}
	
	/*public static void main(String[] args) throws Exception{
		GetEndToEndID("Cancellation_BET");
	}*/
	
	
}
